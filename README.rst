ExifTee
=======

ExifTee is a Python tool to display and modify EXIF metadata.

.. image:: src/exiftee/resources/welcome.png

Manage the project
------------------

Go to the project's root directory.

Run the software
++++++++++++++++

Execute ``python3 manage.py`` with no arguments.

Available targets
+++++++++++++++++

Execute ``python3 manage.py`` with one of the following arguments :

.. code::

    --runtests

Runs all tests.

.. code::

    --sample [name]

Runs a sample from the ``samples`` directory, which have the name ``sample_[name].py``.

.. code::

    --test [name]

Runs a test from the ``tests`` directory, which have the name ``test_[name].py``.

.. code::

    --clean

Removes temporary files, using ``py3clean``.

.. code::

    --i18n

Generates a translation file of user messages, using ``pygettext``. The file will
be located in ``<rootDir>/share/locale``.

Dependencies
------------

Python 3.4 must be installed.

The following libraries are required too :

- tkinter
- Python 3 PIL
- Python 3 PIL ImageTk
- `exiftool <http://owl.phy.queensu.ca/~phil/exiftool/>`_
- `pyexiftool <https://github.com/smarnach/pyexiftool>`_

Install dependencies (tested on Ubuntu 15.04)
+++++++++++++++++++++++++++++++++++++++++++++

.. code::

    # Install Python and additional packages
    sudo apt-get install python3.4
    sudo apt-get install python3-tk

    sudo apt-get install python3-pil
    sudo apt-get install python3-pil.imagetk

    # Install exiftool
    sudo apt-get install libimage-exiftool-perl

    # Install pyexiftool
    cd <someDir>
    git clone https://github.com/smarnach/pyexiftool.git
    python3 pyexiftool/setup.py install

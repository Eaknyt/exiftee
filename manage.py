# -*- coding: utf-8 -*-

import argparse
import os
import subprocess
import sys

sys.path.insert(0, os.path.abspath('src'))
import exiftee

if __name__ == '__main__':
    # Constants
    PYTHON_CMD = 'python3'
    PYTHON_EXT = '.py'

    # Build the argument parser
    parser = argparse.ArgumentParser()

    parser.add_argument('--init', action='store_true',
                        help='download required libraries')

    parser.add_argument('--sample', nargs=1,
                        help='run a specific sample')

    parser.add_argument('--test', nargs=1,
                        help='run a specific test')

    parser.add_argument('--runtests', action='store_true',
                        help='run all tests')

    parser.add_argument('--i18n', action='store_true',
                        help='parse .py files for translatable messages')

    parser.add_argument('--showtree', action='store_true',
                        help='use git to show a tree of the current source')

    parser.add_argument('--clean', action='store_true',
                        help='remove temporary files')

    args = parser.parse_args()

    # The script started with no option, so just run the software
    if len(sys.argv) == 1:
        print("Porting from tkinter to PySide in progress")
        # subprocess.call([PYTHON_CMD, 'src/main' + PYTHON_EXT])
    # Or...
    elif args.init:
        pass
    elif args.sample:
        sampleName = 'sample_' + args.sample[0] + PYTHON_EXT
        subprocess.call([PYTHON_CMD, 'samples/' + sampleName])
    elif args.test:
        testName = 'test_' + args.test[0] + PYTHON_EXT
        subprocess.call([PYTHON_CMD, 'tests/' + testName])
    elif args.runtests:
        subprocess.call([PYTHON_CMD, '-m', 'unittest', 'discover', 'tests'])
    elif args.i18n:
        # Create the i18n folder if it not exists
        i18nDir = exiftee.i18nPath()

        if not os.path.exists(i18nDir):
            os.makedirs(i18nDir)

        # Retrieve Python files list
        path = './src/exiftee'
        translatables = []

        for r, d, f in os.walk(path):
            for entry in f:
                if entry[-3:] == '.py':
                    translatables.append(os.path.join(r, entry))

        dest = os.path.join(i18nDir, 'messages.pot')

        # Extract translatable strings
        subprocess.call(['pygettext', '--keyword=_', '--output='+dest] +
                        translatables)
    elif args.showtree:
        subprocess.call(['git', 'ls-tree', '--full-tree', '-r', 'HEAD'])
    elif args.clean:
        subprocess.call(['py3clean', '.'])

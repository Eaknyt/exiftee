# -*- coding: utf-8 -*-

import tkinter as tk

from context import exiftee
import exiftee.tk


def addProperty(parent, pText):
    frame = tk.Frame(parent)

    tk.Label(frame, text=pText).pack(side=tk.LEFT)
    tk.Entry(frame).pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

    frame.pack(side=tk.TOP, fill=tk.BOTH)


if __name__ == '__main__':
    # Create a foldable widget
    panel = exiftee.tk.Foldable()
    panel.text = 'Foo Foldable'

    # Add some widgets to it
    addProperty(panel.placeholder, 'Property 01')
    addProperty(panel.placeholder, 'Property 02')
    addProperty(panel.placeholder, 'Property 03')
    addProperty(panel.placeholder, 'Property 04')

    panel.pack(side=tk.TOP, fill=tk.BOTH)

    # Create another widget below to verify the geometry allocation
    otherPanel = exiftee.tk.Foldable()
    otherPanel.text = 'Another foldable panel'

    addProperty(otherPanel.placeholder, 'Property 01')
    addProperty(otherPanel.placeholder, 'Property 02')

    otherPanel.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    # event loop
    tk.mainloop()

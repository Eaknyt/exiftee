# -*- coding: utf-8 -*-

import datetime
import gettext
import tkinter as tk

from context import exiftee
import exiftee.tk

if __name__ == '__main__':
    # Well, we need to init the i18n engine
    gettext.install('exiftee', exiftee.i18nPath())

    # Create a path line widget
    de = exiftee.tk.DateEntry()
    de.date = datetime.datetime.today().date()

    de.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    # event loop
    tk.mainloop()

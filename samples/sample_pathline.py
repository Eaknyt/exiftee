# -*- coding: utf-8 -*-

import shutil
import tempfile

from PySide.QtGui import QApplication

from context import exiftee
import exiftee.widgets

def onPathChanged(path):
    pass

if __name__ == '__main__':
    # Create a temp directory and a temp file in it
    tmpDirPath = tempfile.mkdtemp()
    innerDirPath = tempfile.mkdtemp(dir = tmpDirPath)

    # Create the sample app
    app = QApplication([])

    # Create a path line widget
    pl = exiftee.widgets.PathLine()
    pl.path = innerDirPath
    pl.pathChanged.connect(onPathChanged)

    pl.show()

    # event loop
    app.exec_()

    # Clean the resources
    shutil.rmtree(innerDirPath)
    shutil.rmtree(tmpDirPath)

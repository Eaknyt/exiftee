# -*- coding: utf-8 -*-

import tkinter as tk

from context import exiftee
import exiftee.explorers

if __name__ == '__main__':
    tw = exiftee.explorers.DirectoryView()
    tw.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    # event loop
    tk.mainloop()

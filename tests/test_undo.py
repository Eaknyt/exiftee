# -*- coding: utf-8 -*-

import unittest

from PySide.QtGui import QUndoStack
from PySide.QtGui import QUndoCommand

from context import exiftee

import exiftee.undo


class IntWrapper:
    """

    """
    def __init__(self, val):
        self.value = val

    def getValue(self):
        return self.value

    def setValue(self, val):
        if self.value != val:
            self.value = val


class SetIntCommand(QUndoCommand):
    """

    """
    def __init__(self, _intWrapper, _oldVal, _newVal, parent=None):
        QUndoCommand.__init__(self, parent)

        self.wrapper = _intWrapper
        self.oldValue = _oldVal
        self.newValue = _newVal

    def redo(self):
        self.wrapper.setValue(self.newValue)

    def undo(self):
        self.wrapper.setValue(self.oldValue)

    def text(self):
        return "Set Int Value"


class UndoTestCase(unittest.TestCase):
    """

        For each test, an IntWrapper is instanciated with the value 5.
    """
    def setUp(self):
        self.undoStack = QUndoStack()
        self.wrapper = IntWrapper(5)

    def getCurrentVal(self):
        return self.wrapper.getValue()

    def test_basicPush(self):
        cmd1 = SetIntCommand(self.wrapper, self.getCurrentVal(), 2)
        self.undoStack.push(cmd1)

        self.assertEqual(self.undoStack.index(), 1)
        self.assertEqual(self.getCurrentVal(), 2)

    def test_singleUndoRedo(self):
        cmd1 = SetIntCommand(self.wrapper, self.getCurrentVal(), 2)
        self.undoStack.push(cmd1)

        self.undoStack.undo()
        self.assertEqual(self.undoStack.index(), 0)
        self.assertEqual(self.getCurrentVal(), 5)

        self.undoStack.redo()
        self.assertEqual(self.undoStack.index(), 1)
        self.assertEqual(self.getCurrentVal(), 2)

    def test_doubleRedo(self):
        cmd1 = SetIntCommand(self.wrapper, self.getCurrentVal(), 2)
        self.undoStack.push(cmd1)

        self.assertEqual(self.undoStack.index(), 1)
        self.assertEqual(self.getCurrentVal(), 2)

        cmd2 = SetIntCommand(self.wrapper, self.getCurrentVal(), 4)
        self.undoStack.push(cmd2)

        self.assertEqual(self.undoStack.index(), 2)
        self.assertEqual(self.getCurrentVal(), 4)

        self.undoStack.undo()
        self.assertEqual(self.undoStack.index(), 1)
        self.assertEqual(self.getCurrentVal(), 2)

        self.undoStack.undo()
        self.assertEqual(self.undoStack.index(), 0)
        self.assertEqual(self.getCurrentVal(), 5)

        self.undoStack.redo()
        self.assertEqual(self.undoStack.index(), 1)
        self.assertEqual(self.getCurrentVal(), 2)

        self.undoStack.redo()
        self.assertEqual(self.undoStack.index(), 2)
        self.assertEqual(self.getCurrentVal(), 4)

    def test_destructivePush(self):
        #TODO split into smaller test functions
        cmd1 = SetIntCommand(self.wrapper, self.getCurrentVal(), 2)
        self.undoStack.push(cmd1)

        cmd2 = SetIntCommand(self.wrapper, self.getCurrentVal(), 4)
        self.undoStack.push(cmd2)

        self.assertEqual(self.undoStack.index(), 2)
        self.assertEqual(self.getCurrentVal(), 4)

        cmd3 = SetIntCommand(self.wrapper, self.getCurrentVal(), 6)
        self.undoStack.push(cmd3)

        self.assertEqual(self.undoStack.index(), 3)
        self.assertEqual(self.getCurrentVal(), 6)

        self.undoStack.undo()
        self.undoStack.undo()

        self.assertEqual(self.undoStack.index(), 1)
        self.assertEqual(self.getCurrentVal(), 2)

        cmd4 = SetIntCommand(self.wrapper, self.getCurrentVal(), 8)
        self.undoStack.push(cmd4)

        self.assertEqual(self.undoStack.index(), 2)
        self.assertEqual(self.getCurrentVal(), 8)

        self.undoStack.redo()

        self.assertEqual(self.undoStack.index(), 2)
        self.assertEqual(self.getCurrentVal(), 8)

        cmd5 = SetIntCommand(self.wrapper, self.getCurrentVal(), 10)
        self.undoStack.push(cmd5)

        self.assertEqual(self.undoStack.index(), 3)
        self.assertEqual(self.getCurrentVal(), 10)

        self.undoStack.undo()

        self.assertEqual(self.undoStack.index(), 2)
        self.assertEqual(self.getCurrentVal(), 8)

if __name__ == '__main__':
    unittest.main()

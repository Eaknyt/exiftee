# -*- coding: utf-8 -*-

import os.path
import shutil
import tempfile
import unittest

from PySide.QtGui import QApplication

from context import exiftee

import exiftee.widgets


class PathLineTestCase(unittest.TestCase):
    def setUp(self):
        self._app = QApplication([])

    def test_path(self):
        pl = exiftee.widgets.PathLine()

        # Create a temp directory and a temp file in it
        tmpDirPath = tempfile.mkdtemp()
        innerDirPath = tempfile.mkdtemp(dir=tmpDirPath)

        pl.path = innerDirPath

        # Back of one folder
        pl.back()

        self.assertEqual(pl.path, tmpDirPath)

        # Next of one folder
        pl.next(os.path.basename(innerDirPath))
        self.assertEqual(pl.path, innerDirPath)

        # Clean the resources
        shutil.rmtree(innerDirPath)
        shutil.rmtree(tmpDirPath)

        pl.show()

if __name__ == '__main__':
    unittest.main()

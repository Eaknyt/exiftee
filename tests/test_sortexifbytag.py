# -*- coding: utf-8 -*-

import os.path
import PIL.Image
import shutil
import tempfile
import unittest

from context import exiftee

import exiftee.exiftoolext


class SortExifByTagCmdTestCase(unittest.TestCase):
    def _createImages(self):
        """
            Create some images in the temp dir and returns their paths as a
            list.
        """
        ret = []

        for i in range(1, 5):
            path = os.path.join(self._tmpDirPath, 'test' + str(i) + '.jpg')

            img = PIL.Image.new('RGB', (20, 20))
            img.save(path, 'JPEG')

            ret.append(path)

        return ret

    def _initDates(self):
        """
            Set the date of the created images.
        """
        self._FIRST_DATE = '2016:01:01 00:00:00'
        exiftee.exiftoolext.setDate(self._FIRST_DATE, '', [self._files[0]])

        self._SECOND_DATE = '2017:01:01 00:00:00'
        exiftee.exiftoolext.setDate(self._SECOND_DATE, '',
                                    [self._files[1]])

        self._THIRD_DATE = '2018:01:01 00:00:00'
        exiftee.exiftoolext.setDate(self._THIRD_DATE, '', [self._files[2]])

    def setUp(self):
        # Create a temp dir and some empty images
        self._tmpDirPath = tempfile.mkdtemp()

        self._files = self._createImages()
        self._initDates()

    def tearDown(self):
        # Remove the created temp dir and its files
        shutil.rmtree(self._tmpDirPath)

    def test_sortByDate(self):
        ascSorted = exiftee.exiftoolext.sort('EXIF:DateTimeOriginal',
                                             self._files, True)

        lastTagValue = None
        isSorted = True

        for (filePath, tagValue) in ascSorted:
            if lastTagValue and tagValue:
                if lastTagValue > tagValue:
                    isSorted = False
                    break

            lastTagValue = tagValue

        self.assertTrue(isSorted)

    def test_sortByDate_descending(self):
        descSorted = exiftee.exiftoolext.sort('EXIF:DateTimeOriginal',
                                              self._files, False)

        lastTagValue = None
        isSorted = True

        for (filePath, tagValue) in descSorted:
            if lastTagValue and tagValue:
                if lastTagValue < tagValue:
                    isSorted = False
                    break

            lastTagValue = tagValue

        self.assertTrue(isSorted)

if __name__ == '__main__':
    unittest.main()

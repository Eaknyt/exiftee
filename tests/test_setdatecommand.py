# -*- coding: utf-8 -*-

import os.path
import PIL.Image
import shutil
import tempfile
import unittest

from PySide.QtGui import QUndoStack


from context import exiftee

import exiftee.undo
import exiftee.exiftoolext


class SetDateCmdTestCase(unittest.TestCase):
    def _createImages(self):
        """
            Create some images in the temp dir and returns their paths as a
            list.
        """
        ret = []

        for i in range(1, 2):
            path = os.path.join(self._tmpDirPath, 'test' + str(i) + '.jpg')

            img = PIL.Image.new('RGB', (20, 20))
            img.save(path, 'JPEG')

            ret.append(path)

        return ret

    def _initDates(self):
        """
            Set the date of the created images.
        """
        self._ORIGINAL_DATE = '2015:02:25 00:00:00'
        exiftee.exiftoolext.setDate(self._ORIGINAL_DATE, '', self._files)

    def setUp(self):
        """
            Creates an undo stack, init a temporary directory with some empty
            images within.
        """
        self._stack = QUndoStack()

        # Create a temp dir and some empty images
        self._tmpDirPath = tempfile.mkdtemp()

        self._files = self._createImages()
        self._initDates()

    def tearDown(self):
        # Remove the created temp dir and its files
        shutil.rmtree(self._tmpDirPath)
        exiftee.exiftoolext.Manager.instance().finish()

    def test_applyNewDate(self):
        """
            Apply a new date to the test files.
        """
        cmd = exiftee.undo.SetDateCommand('2014:03:10', self._files)
        self._stack.push(cmd)

        et = exiftee.exiftoolext.Manager.instance().getExifTool()

        dateVal = et.get_tag('DateTimeOriginal', self._files[0])
        self.assertEqual(dateVal, '2014:03:10')

        self._stack.undo()

        dateVal = et.get_tag('DateTimeOriginal', self._files[0])
        self.assertEqual(dateVal, self._ORIGINAL_DATE)

    def test_shiftFwDate(self):
        """
            Shift forward the date of the test files.
        """
        cmd = exiftee.undo.SetDateCommand('0002:01:05 0000:00:00',
                                          self._files, '+')

        self._stack.push(cmd)

        et = exiftee.exiftoolext.Manager.instance().getExifTool()

        dateVal = et.get_tag('DateTimeOriginal', self._files[0])
        self.assertEqual(dateVal, '2017:03:30 00:00:00')

        self._stack.undo()

        dateVal = et.get_tag('DateTimeOriginal', self._files[0])
        self.assertEqual(dateVal, self._ORIGINAL_DATE)

    def test_shiftBwDate(self):
        """
            Shift backward the date of the test files.
        """
        cmd = exiftee.undo.SetDateCommand('0004:01:20 0000:00:00',
                                          self._files, '-')

        self._stack.push(cmd)

        et = exiftee.exiftoolext.Manager.instance().getExifTool()

        dateVal = et.get_tag('DateTimeOriginal', self._files[0])
        self.assertEqual(dateVal, '2011:01:05 00:00:00')

        self._stack.undo()

        dateVal = et.get_tag('DateTimeOriginal', self._files[0])
        self.assertEqual(dateVal, self._ORIGINAL_DATE)


if __name__ == '__main__':
    unittest.main()

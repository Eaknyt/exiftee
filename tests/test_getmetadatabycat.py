# -*- coding: utf-8 -*-

import os.path
import PIL.Image
import shutil
import tempfile
import unittest

from context import exiftee

import exiftee.exiftoolext


class GetMetadataByCat(unittest.TestCase):
    def _createImages(self):
        """
            Create some images in the temp dir and returns their paths as a
            list.
        """
        ret = []

        for i in range(1, 3):
            path = os.path.join(self._tmpDirPath, 'test' + str(i) + '.jpg')

            img = PIL.Image.new('RGB', (20, 20))
            img.save(path, 'JPEG')

            ret.append(path)

        return ret

    def setUp(self):
        # Create a temp dir and some empty images
        self._tmpDirPath = tempfile.mkdtemp()

        self._files = self._createImages()

    def tearDown(self):
        # Remove the created temp dir and its files
        shutil.rmtree(self._tmpDirPath)

    def test_grabExifTags(self):
        tagData = {}
        tagData['EXIF:ApertureValue'] = 1
        tagData['EXIF:FocalLength'] = 2

        exiftee.exiftoolext.setTags_batch(tagData, self._files)

        jsonData = exiftee.exiftoolext.getMetadataByCat(['EXIF'], self._files)

        for result in jsonData:
            for (tagName, tagValue) in result.items():
                if tagName == 'SourceFile':
                    continue

                self.assertTrue(tagName.startswith('EXIF:'))

    def test_grabExifAndXmpTags(self):
        tagData = {}
        tagData['EXIF:ApertureValue'] = 1
        tagData['EXIF:FocalLength'] = 2
        tagData['XMP:LensManualDistortionAmount'] = 2
        tagData['XMP:NegativeCacheLargePreviewSize'] = 1

        exiftee.exiftoolext.setTags_batch(tagData, self._files)

        categories = ['EXIF', 'XMP']

        jsonData = exiftee.exiftoolext.getMetadataByCat(categories,
                                                        self._files)

        for result in jsonData:
            for (tagName, tagValue) in result.items():
                if tagName == 'SourceFile':
                    continue

                self.assertTrue(tagName.startswith('EXIF:') or
                                tagName.startswith('XMP:'))


if __name__ == '__main__':
    unittest.main()

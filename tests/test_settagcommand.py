# -*- coding: utf-8 -*-

import os.path
import PIL.Image
import shutil
import tempfile
import unittest

from PySide.QtGui import QUndoStack

from context import exiftee

import exiftee.exiftoolext
import exiftee.undo


class SetTagCmdTestCase(unittest.TestCase):
    def _createImages(self):
        """
            Create some images in the temp dir and returns their paths as a
            list.
        """
        ret = []

        for i in range(1, 5):
            path = os.path.join(self._tmpDirPath, 'test' + str(i) + '.jpg')

            img = PIL.Image.new('RGB', (20, 20))
            img.save(path, 'JPEG')

            ret.append(path)

        return ret

    def _initTags(self):
        """
            Assign the created images with some tags.
        """
        ret = {}

        ret[(self._files[0], 'EXIF:ApertureValue')] = ('', 4.0)
        ret[(self._files[0], 'EXIF:FocalLength')] = ('', 8)

        ret[(self._files[1], 'EXIF:ApertureValue')] = ('', 12)
        ret[(self._files[1], 'EXIF:FocalLength')] = ('', 8)

        return ret

    def setUp(self):
        """
            Creates an undo stack, init a temporary directory with some empty
            images within.
        """
        self._stack = QUndoStack()

        # Create a temp dir and some empty images
        self._tmpDirPath = tempfile.mkdtemp()

        self._files = self._createImages()

    def tearDown(self):
        # Remove the created temp dir and its files
        shutil.rmtree(self._tmpDirPath)

        exiftee.exiftoolext.Manager.instance().finish()

    def test_setScalarTag(self):
        """
            A test function to set some value to a scalar tag from the handled
            files.
        """
        file = self._files[0]
        tagName = 'EXIF:ApertureValue'
        cmdData = {(file, tagName): ('', 8)}

        self._stack.push(exiftee.undo.SetTagCommand(cmdData))

        et = exiftee.exiftoolext.Manager.instance().getExifTool()

        aperture = et.get_tag(tagName, file)

        self.assertEqual(round(aperture), int(8))

        self._stack.undo()
        aperture = et.get_tag(tagName, file)

        self.assertEqual(aperture, None)

    def test_deleteTag(self):
        """
            A test function to delete some tags from the handled files using
            the SetTagCommand class.
        """
        file = self._files[0]
        tagName = 'EXIF:ApertureValue'
        cmdData = {(file, tagName): (3, '')}

        self._stack.push(exiftee.undo.SetTagCommand(cmdData))

        et = exiftee.exiftoolext.Manager.instance().getExifTool()

        aperture = et.get_tag(tagName, file)

        self.assertEqual(aperture, None)

    def test_deleteTags(self):
        """
            A test function to cover the use of the DeleteTagCommand class.
        """
        # Create the tags
        createTagsData = self._initTags()
        self._stack.push(exiftee.undo.SetTagCommand(createTagsData))

        # Remove these tags
        deleteTagsData = createTagsData

        for fileAndTagName, (oldVal, newVal) in deleteTagsData.items():
            deleteTagsData[fileAndTagName] = newVal, oldVal

        self._stack.push(exiftee.undo.DeleteTagCommand(deleteTagsData))

        # Verify
        tagNames = ['EXIF:ApertureValue', 'EXIF:Flash', 'EXIF:FocalLength']
        files = [self._files[0], self._files[1]]

        et = exiftee.exiftoolext.Manager.instance().getExifTool()

        batches = et.get_tags_batch(tagNames, files)

        for tags in batches:
            self.assertTrue(tagNames[0] not in tags)

if __name__ == '__main__':
    unittest.main()

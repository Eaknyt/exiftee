# -*- coding: utf-8 -*-

import unittest

from context import exiftee
import exiftee.core.actionmanager


def onDummyTriggerred():
    pass


class ActionManagerTestCase(unittest.TestCase):
    def onCallbTriggerred(self):
        self.varForCallback = 1

    def setUp(self):
        self.am = exiftee.core.actionmanager.ActionManager()

        self.varForCallback = 0

    def test_dummyAction(self):
        """
            A very basic test.
        """
        ID_DUMMY_ACTION = 'dummyAction'
        self.am.registerAction(ID_DUMMY_ACTION, 'Test.DummyAction',
                               'A dummy action', onDummyTriggerred)

    def test_actionCallback(self):
        """
            Covers the ``exiftee.core.actionmanager.ActionManager.trigger()``
            function.
        """
        ID_CALLB_ACTION = 'callbackAction'
        self.am.registerAction(ID_CALLB_ACTION, 'Test.CallbackAction',
                               'An action with callback',
                               self.onCallbTriggerred)

        self.am.trigger(ID_CALLB_ACTION)

        self.assertEqual(self.varForCallback, 1)

if __name__ == '__main__':
    unittest.main()

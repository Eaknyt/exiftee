Images authors
==============

This document references the icons and images used by exiftee.

- The folder icon used in the photo view was designed by PixelMixer.
http://pixel-mixer.com
http://www.softicons.com/toolbar-icons/basic-icons-by-pixelmixer/folder-icon

- The France map image used in the gps view was found here :
https://en.wikipedia.org/wiki/Module:Location_map/data/France

exiftee - Manual
================

Welcome to the exiftee manual.

The following instructions covers the full use of the software.

Introduction
------------

exiftee allows you to view the metadatas of photos and edit them with useful
tools.

Once the software started, two windows appear.

The above is the Welcome dialog. You can hide it during the next startups by
uncheck the bottom-left check box.

Choose a working directory
--------------------------

The photo view is the central part of the interface.

It displays the elements of a given directory (the working directory).

This directory can be set using the left panel of the UI. Double-click on a
folder name to set it as the current working directory.

You can add favorites folders using the Favorites tab or the shortcut
Control + D.

Navigating within the Photo View
--------------------------------

The photo view displays images and folders contained within the working
directory.

You can double-click on a folder set it as the current working directory.

Selecting image files
---------------------

In the photo view, left-click to select an image. Use the Control key to select
and unselect more photos.

The shortcut Control + A select all elements of the working directory.

Reviewing and editing metadatas
-------------------------------

The right part of the interface gives you informations about the EXIF
metadatas.

The Metadatas tab, called the tag view, displays the EXIF tags.

A double click on a tag in the tag view allows you to edit its value.

Locating the coordinates of a photo
-----------------------------------

If the selected image contains GPS coordinates located in France, the Gps View
tab displays these coordinates as a red circle.

User modes
----------

Two modes are availables : Basic and Advanced.

EXIF metadatas are grouped into categories.

In the first mode, the tag view displays the tags from the EXIF category.

In the other, all categories are displayed.

You can change the user mode using the Preferences tool (see below).

Tools
-----

The following tools are accessibles from the Tools menu.

Deleting metadatas
++++++++++++++++++

Before uploading a photo to a website, you may want to erase some metadatas
from it, like the GPS coordinates, or the brand of your device.

Go to the menu Tools | Delete metadatas.

Click on tag name and it will be deleted if you click on the button Delete
selected tags. Click again and it will not be deleted anymore.

This action is undoable using the Edit | Undo menu or the Control + Z shortcut.

If you want to delete all metadatas, just press the Delete ALL tags button.
Beware, this action is not undoable.

Modifying the date of a photo
+++++++++++++++++++++++++++++

You can modify the date of a photo selection.

Open the tool using the Tools | Modify date menu.

The open dialog offers you two modes :

- the first allows you to change the date.
- the second allows you to shift the date with a given datetime.

Renaming photos
+++++++++++++++

Open the tool using the Tools | Rename pictures menu.

Use the text area at the left to compose a renaming pattern. You can insert
some tag values using the right panel.

Preferences
+++++++++++

Accessible from the menu Tool | Preferences, or the shortcut Control + P.

Set the shortcuts of your favorites commands or change the user mode.

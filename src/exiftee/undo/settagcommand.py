# -*- coding: utf-8 -*-

import exiftee.exiftoolext

from PySide.QtGui import QUndoCommand


class SetTagCommand(QUndoCommand):
    def __init__(self, data, parent=None):
        """
            Constructs a ``SetTagCommand`` object.

            'ops' is a dictionary in which each entry associates a
            ``(file, tag,)`` with a ``(oldValue, newValue)`` tuple.
        """
        QUndoCommand.__init__(self, parent)
        self._data = data

    def text(self):
        return _('Set tags')

    def undo(self):
        undoData = {}

        for (file, tag), (oldVal, newVal) in self._data.items():
            undoData[(file, tag)] = oldVal

        exiftee.exiftoolext.setTags(undoData)

    def redo(self):
        redoData = {}

        for (file, tag), (oldVal, newVal) in self._data.items():
            redoData[(file, tag)] = newVal

        exiftee.exiftoolext.setTags(redoData)

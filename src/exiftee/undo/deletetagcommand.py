# -*- coding: utf-8 -*-

from exiftee.undo import SetTagCommand


class DeleteTagCommand(SetTagCommand):
    def __init__(self, data, parent=None):
        """
            Constructs a ``DeleteTagCommand`` object.

            Like the ``SetTagCommand``, 'data' must be a dictionary in which
            each entry associates a tuple ``(file, tagName)`` as key, but this
            time we'll prefer to associate it to a ``(oldValue, '')`` tuple.
        """
        super().__init__(data, parent)

    def text(self):
        return _('Delete tags')

# -*- coding: utf-8 -*-

import exiftee.exiftoolext

from PySide.QtGui import QUndoCommand


class SetDateCommand(QUndoCommand):
    def __init__(self, newDate, files, shift='', parent=None):
        """
            Constructs a ``SetDateCommand`` object.

            This command will set the DateTimeOriginal tag from the date of
            each file contained in 'files' to 'newDate'.

            'newDate' must be a date time object or a date shift formatted
            like : http://www.sno.phy.queensu.ca/~phil/exiftool/faq.html#Q5

            'files' is a list containing file paths.

            Shifting
            --------

            To control the kind of shifting to apply to the current date,
            set the value of 'shift' to '+' or '-', or use the 'SHIFT_FW' and
            'SHIFT_BW' named constants. Leave 'shift' to its default value to
            disable the shifting.
        """
        QUndoCommand.__init__(self, parent)

        # The three constants that may be passed during the object creation
        #  to determine if a date shift must be applied to the current date.
        self.NO_SHIFT = ''
        self.SHIFT_FW = '+'
        self.SHIFT_BW = '-'

        self._newDate = newDate
        self._files = files
        self._fileDates = exiftee.exiftoolext.getDates(self._files)
        self._shiftMode = shift

    def text(self):
        return _('Modify date')

    def undo(self):
        for (file, date) in self._fileDates.items():
            exiftee.exiftoolext.setDate(date, self.NO_SHIFT,
                                        [file])

    def redo(self):
        exiftee.exiftoolext.setDate(self._newDate, self._shiftMode,
                                    list(self._fileDates.keys()))

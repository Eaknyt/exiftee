# -*- coding: utf-8 -*-

"""
    The ``exiftee.widgets`` package
    ======================

    The ``exiftee.widgets`` package contains custom widgets created with PySide.
"""

from exiftee.widgets.pathline import PathLine


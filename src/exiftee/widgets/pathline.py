# -*- coding: utf-8 -*-

import os
import os.path

from PySide.QtGui import QFrame
from PySide.QtGui import QHBoxLayout
from PySide.QtGui import QToolButton
from PySide.QtCore import Signal


class PathLine(QFrame):
    """
        The ``PathLine`` widget displays the full path of a given directory
        and provides interactive navigation to the user.

        This widget is basically implemented as a frame containing a set of
        horizontally layered buttons. Each directory that composes the current
        path is represented by a button.

        If the user click on a button, .

        A PathLine has the following properties :
        - path is the currently displayed path. It is stored as a string.
    """

    pathChanged = Signal(str)

    def __init__(self, parent=None):
        QFrame.__init__(self, parent)

        self._layout = QHBoxLayout(self)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.setSpacing(0)

        self._path = ''
        self.path = os.sep

    def _updateContent(self):
        """
            Redraw the PathLine's content using the current path.
        """
        # Clear the frame's content
        while self._layout.count():
            layItem = self._layout.takeAt(0)

            if layItem:
                widget = layItem.widget()
                if widget:
                    widget.deleteLater()

        # Retrieve directories names in a list
        driveAndPath = os.path.splitdrive(self.path)
        directories = driveAndPath[1].split(os.sep)
        currentDir = directories[-1]

        if not directories[0]:
            directories[0] = '/'

        if len(directories) == 2 and not directories[1]:
            del directories[1]

        # Populate the layout
        for dirName in directories:
            btn = QToolButton(self)
            btn.setText(dirName)

            self._layout.addWidget(btn)
            if dirName == directories[-1]:
                self._layout.addStretch()

            slot = lambda btnName=dirName:self._onButtonClicked(btnName)
            btn.clicked.connect(slot)

            if dirName == currentDir:
                btn.setChecked(True)

    def _onButtonClicked(self, btnName):
        """
            This handler is automatically called when a button of the PathLine
            is clicked.

            Use ``btnName`` to retrieve the text of this button.
        """
        idx = self.path.find(btnName)
        assert(idx != -1)

        newPath = self.path[: idx + len(btnName)]
        assert(os.path.isabs(newPath))

        self.path = newPath

    @property
    def path(self):
        """
            The path property is the path currently displayed by the
            PathLine widget.

            Its setter takes a ``path`` argument and handles both absolute and
            relative paths :

            If ``path`` is a file, the widget displays its parent directory's
            path.
            If ``path`` is invalid, the widget displays the current drive's
            root's path.
        """
        return self._path

    @path.setter
    def path(self, newPath):
        if self._path != newPath:
            if os.path.isabs(newPath) and os.path.isdir(newPath):
                self._path = newPath

                self._updateContent()

                self.pathChanged.emit(newPath)
            else:
                raise NotADirectoryError(newPath + ' is not a directory.')

    @path.deleter
    def path(self):
        self._path = os.sep

    def back(self):
        """
            Set the current path to the parent folder.
        """
        joined = os.path.join(self._path, '..')
        self.path = os.path.normpath(joined)

    def next(self, dirName):
        """
            Set the current path to the folder named as ``dirName``.
        """
        joined = os.path.join(self._path, dirName)
        self.path = os.path.normpath(joined)

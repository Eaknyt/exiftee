# -*- coding: utf-8 -*-

import os
import os.path
import tkinter as tk
import tkinter.ttk as ttk

import exiftee.exifviews
import exiftee.exiftoolext
import exiftee.tk


class DeleteTagView(tk.Frame):
    """

    """
    def __init__(self, parent=None, **kwargs):
        tk.Frame.__init__(self, parent, **kwargs)

        self._files = []
        self._tagNames = []

        self._checkedItems = []

        self._setupUI()

    def _setupUI(self):
        # Create the filtering entry
        filtererFrame = tk.Frame(self)
        filtererFrame.pack(side=tk.TOP, fill=tk.X)

        filtererLabel = tk.Label(filtererFrame, text=_('Tag filter'))
        filtererLabel.pack(side=tk.LEFT)

        # Create the tree view
        self._COL_TAG_ID = 'colTag'
        self._COL_RM_ID = 'colRm'

        cols = (self._COL_TAG_ID, self._COL_RM_ID)

        self._tree = exiftee.tk.CTreeView(self, columns=cols,
                                          displaycolumns='#all',
                                          selectmode=tk.EXTENDED)

        filterEntry = exiftee.tk.ValidateEntry(filtererFrame,
                                               textvariable=self._tree.filterVar)
        filterEntry.validateCallback = self._validateFilter

        filterEntry.pack(side=tk.LEFT, fill=tk.X, expand=True)

        self._tree.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        self._tree.heading(self._COL_TAG_ID, text=_('Tag'))
        self._tree.heading(self._COL_RM_ID, text=_('Delete'))

        # Bind the tree events
        self._tree.bind('<<TreeviewSelect>>', self._onItemSelected)

    def _validateFilter(self, supposed, inserted):
        return inserted.isalpha()

    def _updateModel(self):
        newModel = []

        if self._tagNames:
            for (fullTag, ) in self._tagNames:
                splitted = fullTag.split(':')

                # The tag has a group
                if len(splitted) > 1:
                    group = splitted[0]
                    name = splitted[1]
                # or not
                else:
                    group = _('Other')
                    name = splitted[0]

                # Insert the parent row if necessary
                parentRow = ('', group, group)

                if parentRow not in newModel:
                    newModel.append(parentRow)

                if fullTag in self._checkedItems:
                    row = (group, fullTag, name, 'x')
                else:
                    row = (group, fullTag, name)

                newModel.append(row)

            self._tree.model.source = newModel

        else:
            del self._tree.model.source

    def _retrieveTagNames(self):
        self._tagNames = exiftee.exiftoolext.getTagNames(self._files)

    def _childrenAreChecked(self, item):
        children = self._tree.get_children(item)

        if not children:
            return False

        for child in children:
            if child not in self._checkedItems:
                return False

        return True

    def _checkItem(self, item, select):
        if not item:
            return

        if self._tree.get_children(item):
            return

        if not select:
            self._tree.set(item, self._COL_RM_ID, '')

            if item in self._checkedItems:
                self._checkedItems.remove(item)
        else:
            self._tree.set(item, self._COL_RM_ID, 'x')

            if item not in self._checkedItems:
                self._checkedItems.append(item)

    def _recursiveSelect(self, item, select):
        self._checkItem(item, select)

        for child in self._tree.get_children(item):
            self._recursiveSelect(child, select)

    def _onItemSelected(self, event=None):
        selectedItem = self._tree.focus()

        select = (not self._childrenAreChecked(selectedItem) and
                  selectedItem not in self._checkedItems)

        self._recursiveSelect(selectedItem, select)

    def clear(self):
        """
            Removes all files and all tags handled by the tag name view.
        """
        self.files = []

    @property
    def files(self):
        """
            The files property defines the files whose exif tags are displayed
            by the tag name view.
        """
        return self._files

    @files.setter
    def files(self, newFiles):
        fileList = []

        for f in newFiles:
            if os.path.isdir(f):
                entries = os.listdir(f)

                for e in entries:
                    ePath = os.path.join(f, e)
                    if not os.path.isdir(ePath):
                        fileList.append(ePath)
            else:
                fileList.append(f)

        if self._files != fileList:
            self._files = fileList

            self._retrieveTagNames()

            # Update the tree view
            self._updateModel()
            self._tree.update()

    @files.deleter
    def files(self):
        self._files.clear()

    def checkedTags(self):
        return self._checkedItems

# -*- coding: utf-8 -*-

import os
import os.path
import tkinter as tk
import tkinter.ttk as ttk

import exiftee.exifviews
import exiftee.exiftoolext
import exiftee.core.settings
import exiftee.tk


class TagView(tk.Frame):
    """
        The ``TagView`` class provides a tree view to display exif metadatas
        from a bunch of files.

        The tag view can handle several files but can only display one at the
        same time.

        The files property is a list of image paths. When this property
        changes, the tag view retrieve all metadatas. Then the view displays
        those of the first file in the list by default.

        The index property can be used to decide which file in the list the tag
        view must display.

        The tag view supports sorting and filtering. These criteria are defined
        by the ``TagViewSortFilterModel`` class from the ``exiftee.tk``
        package.
    """
    def __init__(self, parent, settings, **kwargs):
        tk.Frame.__init__(self, parent, **kwargs)

        self._theSettings = settings

        self._files = []
        self._metadatas = {}
        self._index = 0

        self.callbacks = {
            'tagDoubleClicked': None
        }

        self._setupUI()

    def _setupUI(self):
        # Create the filtering entry
        filtererFrame = tk.Frame(self)
        filtererFrame.pack(side=tk.TOP, fill=tk.X)

        filtererLabel = tk.Label(filtererFrame, text=_('Tag filter'))
        filtererLabel.pack(side=tk.LEFT)

        # Create the tree view
        #  Column ids
        self._COL_TAG_ID = 'colTag'
        self._COL_VAL_ID = 'colVal'

        cols = (self._COL_TAG_ID, self._COL_VAL_ID)

        self._tree = exiftee.tk.CTreeView(self, columns=cols,
                                          displaycolumns='#all',
                                          selectmode=tk.BROWSE)

        filterEntry = exiftee.tk.ValidateEntry(filtererFrame,
                                               textvariable=self._tree.filterVar)
        filterEntry.validateCallback = self._validateFilter

        filterEntry.pack(side=tk.LEFT, fill=tk.X, expand=True)

        self._tree.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        self._tree.heading(self._COL_TAG_ID, text=_('Tag'))
        self._tree.heading(self._COL_VAL_ID, text=_('Value'))

        self._tree.bind('<Double-1>', self._onItemDoubleClicked)

    def _validateFilter(self, supposed, inserted):
        return inserted.isalpha()

    def _updateModel(self):
        newModel = []

        if self._metadatas:
            asList = list(self._metadatas[self._index].items())

            for (fullTag, value) in asList:
                splitted = fullTag.split(':')

                # The tag has a group
                if len(splitted) > 1:
                    group = splitted[0]
                    name = splitted[1]
                # or not
                else:
                    group = _('Other')
                    name = splitted[0]

                # Insert the parent row if necessary
                parentRow = ('', group, group)

                if parentRow not in newModel:
                    newModel.append(parentRow)

                row = (group, fullTag, name, value)
                newModel.append(row)

            self._tree.model.source = newModel
        else:
            del self._tree.model.source

    def _retrieveMetadata(self):
            if self._files:
                cats = self._theSettings.modeCategories()
                self._metadatas = exiftee.exiftoolext \
                                         .getMetadataByCat(cats, self._files)
            else:
                self._metadatas.clear()

    def _onItemDoubleClicked(self, event=None):
        selectedItem = self._tree.selection()[0]

        if self._tree.get_children(selectedItem):
            return

        tagName = selectedItem
        value = self._tree.item(selectedItem, 'values')[1]

        if self.callbacks['tagDoubleClicked']:
            self.callbacks['tagDoubleClicked'](tagName, value)

    def clear(self):
        """
            Removes all files and all tags handled by the tag view.
        """
        self.files = []

    def update(self):
        """
            Queries the metadata from the currently handled files and update
            the tree view.

            Use this function when you're sure that the exif metadata has
            changed in the handled files.
        """
        self._retrieveMetadata()
        self._updateModel()
        self._tree.update()

    @property
    def files(self):
        """
            The files property defines the files whose exif tags are displayed
            by the tag view.

            Depending on the value of the ``processDirs`` attribute, the new
            list will be processed differently. If it's true, the files
            contained in each folder of the list will be added to the current
            file list. Otherwise the folders are not added to the current file
            list.
        """
        return self._files

    @files.setter
    def files(self, newFiles):
        fileList = []

        fileList = [f for f in newFiles if not os.path.isdir(f)]

        if self._files != fileList:
            self._files = fileList

            self.update()

    @files.deleter
    def files(self):
        self._files.clear()

    @property
    def index(self):
        """
            The index property defines the file whose metadatas are currently
            displayed by the tag view.
        """
        return self._index

    @index.setter
    def index(self, newIndex):
        if (self._index != newIndex) and (self._index < len(self._files)):
            self._index = newIndex

            self._updateModel()
            self._tree.update()

    def currentFile(self):
        """
            Returns the path of the photo currently displayed in the tag view.
        """
        if not self._files:
            return ''

        return self._files[self._index]

    def getGpsCoords(self):
        currentDatas = self._metadatas[self._index]

        gpsTags = exiftee.exiftoolext.gpsTagNames()

        coords = dict(filter(lambda i: i[0] in gpsTags, currentDatas.items()))
        ret = None

        if (len(coords) == 4):
            if coords['EXIF:GPSLatitudeRef'] == 'S':
                lat = -coords['EXIF:GPSLatitude']
            else:
                lat = coords['EXIF:GPSLatitude']

            if coords['EXIF:GPSLongitudeRef'] == 'W':
                longt = -coords['EXIF:GPSLongitude']
            else:
                longt = coords['EXIF:GPSLongitude']

            ret = (lat, longt)

        return ret

# -*- coding: utf-8 -*-

import tkinter as tk
import tkinter.ttk as ttk

import exiftee.tk


class GeneralSettingsPage(tk.Frame):
    def __init__(self, parent, app, **kwargs):
        tk.Frame.__init__(self, parent, **kwargs)

        self._app = app

        self._setupUI()

    def _setupUI(self):
        modeLabel = tk.Label(self, text=_('User mode') + ' : ')
        modeLabel.grid(row=0, column=0)

        self._modeVar = tk.StringVar()
        modeComboBox = ttk.Combobox(self, textvariable=self._modeVar,
                                    values=self._app._settings.modes())
        modeComboBox.grid(row=0, column=1)
        modeComboBox.set(self._app._settings.mode)

    def apply(self, settings):
        settingsChanged = False

        newMode = self._modeVar.get()

        if newMode != settings.mode:
            settings.mode = newMode
            self._app.setMode(newMode)

            settingsChanged = True

        if settingsChanged:
            settings.save()


class ShortcutsSettingsPage(tk.Frame):
    def __init__(self, parent, app, **kwargs):
        tk.Frame.__init__(self, parent, **kwargs)

        self._app = app
        self._am = app.am

        self._setupUI()

    def _setupUI(self):
        # Create the filtering entry
        filtererFrame = tk.Frame(self)
        filtererFrame.pack(side=tk.TOP, fill=tk.X)

        filtererLabel = tk.Label(filtererFrame, text=_('Filter'))
        filtererLabel.pack(side=tk.LEFT)

        # Create the tree view
        self._COL_ACT_NAME_ID = 'colActionName'
        self._COL_ACT_TEXT_ID = 'colActionText'
        self._COL_ACT_SHORTCUT_ID = 'colActionShortcut'

        cols = (self._COL_ACT_NAME_ID, self._COL_ACT_TEXT_ID,
                self._COL_ACT_SHORTCUT_ID)

        self._tree = exiftee.tk.CTreeView(self, columns=cols,
                                          displaycolumns='#all',
                                          selectmode=tk.EXTENDED)

        filterEntry = exiftee.tk.ValidateEntry(filtererFrame,
                                               textvariable=self._tree.filterVar)

        filterEntry.pack(side=tk.LEFT, fill=tk.X, expand=True)

        self._tree.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        self._tree.heading(self._COL_ACT_NAME_ID, text=_('Name'))
        self._tree.heading(self._COL_ACT_TEXT_ID, text=_('Text'))
        self._tree.heading(self._COL_ACT_SHORTCUT_ID, text=_('Key sequence'))

        self._tree.bind('<<TreeviewSelect>>', self._onActionSelected)

        self._tree.model.enableSorting = False

        # Create the shortcut entry
        shortcutConfigFrame = tk.Frame(self)
        shortcutConfigFrame.pack(side=tk.TOP, fill=tk.X)

        shortcutEntryLabel = tk.Label(shortcutConfigFrame,
                                      text=_('Shortcut') + ' :')
        shortcutEntryLabel.pack(side=tk.LEFT)

        self._shortcutEntryVar = tk.StringVar()

        self._shortcutEntry = tk.Entry(shortcutConfigFrame, state=tk.DISABLED,
                                       textvariable=self._shortcutEntryVar)
        self._shortcutEntry.pack(side=tk.LEFT, fill=tk.X, expand=True)

        self._shortcutEntry.bind('<KeyRelease-Return>', self._onShortcutEdited)

        self._editShortcutBtn = tk.Button(shortcutConfigFrame, text=_('Edit'),
                                          command=self._onShortcutEdited,
                                          state=tk.DISABLED)
        self._editShortcutBtn.pack(side=tk.LEFT)

        # Fill the tree
        self._retrieveActions()
        self._tree.update()

    def _retrieveActions(self):
        newModel = []

        parents = []

        for (actionId, action) in self._am.actions.items():
            parentRow = ('', action.context, action.context)

            if parentRow not in parents:
                parents.append(parentRow)

            row = (action.context, actionId, actionId,
                   action.text, self._am.readableShortcut(action.keySeq))
            newModel.append(row)

        for p in parents:
            newModel.insert(0, p)

        self._tree.model.source = newModel

    def _currentAction(self):
        selectedItem = self._tree.focus()

        return self._tree.item(selectedItem, 'values')[0]

    def _onActionSelected(self, event=None):
        actionId = self._currentAction()

        if actionId not in self._am.actions:
            self._shortcutEntry.config(state=tk.DISABLED)
            self._editShortcutBtn.config(state=tk.DISABLED)
            self._shortcutEntryVar.set('')
        else:
            self._shortcutEntry.config(state=tk.NORMAL)
            self._editShortcutBtn.config(state=tk.NORMAL)

            action = self._am.actions[actionId]
            shortcut = self._am.readableShortcut(action.keySeq)

            self._shortcutEntryVar.set(shortcut)

    def _onShortcutEdited(self, event=None):
        selectedItem = self._tree.focus()
        newShortcut = self._shortcutEntryVar.get()

        self._tree.set(selectedItem, self._COL_ACT_SHORTCUT_ID, newShortcut)

    def apply(self, settings):
        settingsChanged = False

        for (actionId, action) in self._am.actions.items():
            currentShortcut = self._am.readableShortcut(action.keySeq)
            newShortcut = self._tree.set(actionId, self._COL_ACT_SHORTCUT_ID)

            if currentShortcut != newShortcut:
                settings.customShortcuts[actionId] = newShortcut

                newKeySeq = self._am.fromReadableShortcut(newShortcut)
                self._am.setKeySeq(actionId, newKeySeq)

                settingsChanged = True

        if settingsChanged:
            settings.save()


class SettingsDialog(tk.Toplevel):
    def __init__(self, parent):
        tk.Toplevel.__init__(self, parent)

        self._app = parent
        self._theSettings = self._app._settings
        self.title(_('Settings'))

        self._setupUI()

        self.protocol('WM_DELETE_WINDOW', self.cancel)
        self._canceled = False

        self.grab_set()

        parent.wait_window(self)

    def _setupUI(self):
        mainFrame = tk.Frame(self)
        mainFrame.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        # Create the left panel, where the user can switch between pages
        leftPanel = tk.Frame(mainFrame)
        leftPanel.pack(side=tk.LEFT, fill=tk.Y)

        self._pageListWidget = tk.Listbox(leftPanel)
        self._pageListWidget.bind('<<ListboxSelect>>', self._onPageSelected)
        self._pageListWidget.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        pageFrame = tk.Frame(mainFrame)
        pageFrame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        self._pageTitleLabel = ttk.Label(pageFrame, font='Helvetica 16 bold')
        self._pageTitleLabel.pack(side=tk.TOP, fill=tk.X, padx=5)

        pageContainer = tk.Frame(pageFrame)
        pageContainer.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        pageContainer.grid_rowconfigure(0, weight=1)
        pageContainer.grid_columnconfigure(0, weight=1)

        # Create each pages
        self._pages = {
            _('General'): GeneralSettingsPage(pageContainer, self._app),
            _('Shortcuts'): ShortcutsSettingsPage(pageContainer, self._app)
        }

        self._pagesOrder = [_('General'), _('Shortcuts')]

        for title in self._pagesOrder:
            pageWidget = self._pages[title]
            self._pageListWidget.insert(tk.END, title)
            pageWidget.grid(row=0, column=0, sticky='nsew')

        # Create the buttons
        btnsFrame = tk.Frame(self)

        deleteTagsBtn = ttk.Button(btnsFrame, text=_('Ok'),
                                   command=self.ok)
        deleteTagsBtn.pack(side=tk.LEFT, expand=True)

        cancelBtn = ttk.Button(btnsFrame, text=_('Cancel'),
                               command=self.cancel)
        cancelBtn.pack(side=tk.LEFT, expand=True)

        applyBtn = ttk.Button(btnsFrame, text=_('Apply'),
                              command=self.apply)
        applyBtn.pack(side=tk.LEFT, expand=True)

        btnsFrame.pack(side=tk.TOP, fill=tk.X, pady=10)

        # Select the first settings page
        self._pageListWidget.selection_set(0)
        self._pageListWidget.event_generate('<<ListboxSelect>>')

    def _onPageSelected(self, event=None):
        pageIdx = self._pageListWidget.curselection()[0]
        pageName = self._pageListWidget.get(pageIdx)

        self._pageTitleLabel.config(text=pageName)

        pageWidget = self._pages[pageName]
        pageWidget.tkraise()

    def _close(self):
        self._app.focus_set()
        self.destroy()

    def ok(self):
        self.apply()
        self._close()

    def cancel(self):
        self._canceled = True
        self._close()

    def apply(self):
        for (title, pageWidget) in self._pages.items():
            pageWidget.apply(self._theSettings)

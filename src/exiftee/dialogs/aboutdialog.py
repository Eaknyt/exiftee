# -*- coding: utf-8 -*-

import tkinter as tk


class AboutDialog(tk.Toplevel):
    def __init__(self, parent):
        tk.Toplevel.__init__(self, parent)

        self._app = parent
        self.title(_('About'))

        self._setupUI()

        self.protocol('WM_DELETE_WINDOW', self.ok)

        self.grab_set()

        parent.wait_window(self)

    def _setupUI(self):
        aboutText = """
        exiftee is a tool to display and edit EXIF tags, mainly developed for a
        university project.

        Languages & libraries :
        - Python 3.4
        - tkinter
        - Python 3 PIL
        - Python 3 PIL ImageTk
        - exiftool
        - pyexiftool

        Developer : Eaknyt

        Project repository : https://github.com/Eaknyt/exiftee/
        """

        label = tk.Label(self, text=aboutText)
        label.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        closeBtn = tk.Button(self, text=_('Close'), command=self.ok)
        closeBtn.pack(side=tk.TOP)

    def _close(self):
        self._app.focus_set()
        self.destroy()

    def ok(self):
        self._close()

# -*- coding: utf-8 -*-

"""
    The ``exiftee.dialogs`` package
    ===============================

"""

from exiftee.dialogs.dialogs import askDate
from exiftee.dialogs.dialogs import askRenamingPattern
from exiftee.dialogs.dialogs import askTagsToDelete
from exiftee.dialogs.dialogs import askTagValue

from exiftee.dialogs.aboutdialog import AboutDialog
from exiftee.dialogs.settingsdialog import SettingsDialog
from exiftee.dialogs.welcomedialog import WelcomeDialog

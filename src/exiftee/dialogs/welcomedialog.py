# -*- coding: utf-8 -*-

import PIL
import tkinter as tk
import tkinter.ttk as ttk


class WelcomeDialog(tk.Toplevel):
    def __init__(self, parent):
        tk.Toplevel.__init__(self, parent)

        self._app = parent
        self.title(_('Welcome'))

        self._setupUI()

        self.protocol('WM_DELETE_WINDOW', self.ok)

        self.grab_set()

        parent.wait_window(self)

    def _setupUI(self):
        # Image and informations
        mainFrame = tk.Frame(self)
        mainFrame.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        welcomeText = _("""
        Welcome to exiftee !


        """)

        welcomeLabel = tk.Label(mainFrame, text=welcomeText,
                                font='Helvetica 20 bold')
        welcomeLabel.pack(side=tk.TOP, fill=tk.X)

        image = PIL.Image.open('src/exiftee/resources/welcome.png')
        image.thumbnail((900, 700), PIL.Image.ANTIALIAS)

        photoImg = PIL.ImageTk.PhotoImage(image)

        imageLabel = tk.Label(mainFrame, image=photoImg, border=1, bg='black')
        imageLabel.image = photoImg
        imageLabel.pack(side=tk.TOP, padx=4)

        # Ok button
        bottomFrame = tk.Frame(self)
        bottomFrame.pack(side=tk.TOP, fill=tk.X)

        modeLabel = tk.Label(bottomFrame, text=_('User mode') + ' : ')
        modeLabel.pack(side=tk.LEFT)

        self._modeVar = tk.StringVar()
        modeComboBox = ttk.Combobox(bottomFrame, textvariable=self._modeVar,
                                    values=self._app._settings.modes())
        modeComboBox.pack(side=tk.LEFT)
        modeComboBox.set(self._app._settings.mode)

        self._openAtNextStartVar = tk.IntVar()
        self._openAtNextStartVar.set(self._app._settings.openWelcome)

        cboxText = _('See this at the next startup')

        openAtNextStartCBox = tk.Checkbutton(bottomFrame,
                                             text=cboxText,
                                             variable=self._openAtNextStartVar)
        openAtNextStartCBox.pack(side=tk.LEFT)

        closeBtn = tk.Button(bottomFrame, text=_('Close'), command=self.ok)
        closeBtn.pack(side=tk.RIGHT)

    def _close(self):
        # Save preferences
        self._app._settings.openWelcome = self._openAtNextStartVar.get()
        self._app._settings.mode = self._modeVar.get()
        self._app._settings.save()

        # Close
        self._app.focus_set()
        self.destroy()

    def ok(self):
        self._close()

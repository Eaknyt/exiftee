# -*- coding: utf-8 -*-

import exiftee.dialogs.private


def askTagsToDelete(parent, files):
    d = exiftee.dialogs.private._DeleteTagDialog(parent, files)

    return d.tags()


def askDate(parent, currentDate=None):
    """
        Opens a dialog to allow the user to select a date using three combo
        boxes or to create a date shift.

        'currentDate' must be a datetime object and can be set to display an
        existing date in the dialog.
    """
    d = exiftee.dialogs.private._SetDateDialog(parent, currentDate)

    return d.dateTime()


def askRenamingPattern(parent, files):
    d = exiftee.dialogs.private._RenameDialog(parent, files)

    return d.pattern()


def askTagValue(parent, tag, currentValue):
    d = exiftee.dialogs.private._EditTagDialog(parent, tag, currentValue)

    return d.value()

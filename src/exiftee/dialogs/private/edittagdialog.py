# -*- coding: utf-8 -*-

import tkinter as tk
import tkinter.ttk as ttk

import exiftee.exiftoolext


class _EditTagDialog(tk.Toplevel):
    def __init__(self, parent, tag, value):
        tk.Toplevel.__init__(self, parent)

        self._parent = parent
        self._tag = tag
        self._value = value

        self._isEnum = tag in exiftee.exiftoolext.exifEnumTags
        if self._isEnum:
            self._possibleValues = exiftee.exiftoolext.exifEnumTags[tag]

        self.title(_('Edit tag'))

        self._setupUI()

        self.protocol('WM_DELETE_WINDOW', self.cancel)
        self._canceled = False

        # Because we all love consistency
        #self.grab_set()

        parent.wait_window(self)

    def _isInt(self, var):
        try:
            int(var)
            return True
        except ValueError:
            return False

    def _isFloat(self, var):
        try:
            float(var)
            return True
        except ValueError:
            return False

    def _enumRawValue(self, value):
        for (rawVal, prVal) in self._possibleValues.items():
            if prVal == value:
                return rawVal

        return None

    def _createEditor(self, parent, variable, value):
        ret = None

        FROM = -10000
        TO = 10000

        if self._isEnum:
            vals = list(self._possibleValues.values())

            ret = ttk.Combobox(parent, textvariable=variable,
                               values=vals)
        elif self._isInt(value):
            ret = tk.Spinbox(parent, textvariable=variable, from_=FROM, to=TO,
                             increment=1)
        elif self._isFloat(value):
            ret = tk.Spinbox(parent, textvariable=variable, from_=FROM, to=TO,
                             increment=0.1)
        else:
            ret = tk.Entry(parent, textvariable=variable)

        return ret

    def _setupUI(self):
        # Create the editor
        editorFrame = tk.Frame(self)

        tagNameLabel = tk.Label(editorFrame, text=self._tag + ' : ')
        tagNameLabel.pack(side=tk.LEFT)

        self._editorVar = tk.StringVar()

        if self._isEnum:
            if self._isInt(self._value):
                self._value = int(self._value)

            enumValue = self._possibleValues[self._value]

            self._editorVar.set(enumValue)
        else:
            self._editorVar.set(self._value)

        self._editor = self._createEditor(editorFrame, self._editorVar,
                                          self._value)
        self._editor.pack(side=tk.LEFT, expand=True)

        editorFrame.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        # Create the buttons
        btnsFrame = tk.Frame(self)

        deleteTagsBtn = ttk.Button(btnsFrame, text=_('Ok'),
                                   command=self.ok)
        deleteTagsBtn.pack(side=tk.LEFT, expand=True)

        cancelBtn = ttk.Button(btnsFrame, text=_('Cancel'),
                               command=self.cancel)
        cancelBtn.pack(side=tk.LEFT, expand=True)

        btnsFrame.pack(side=tk.TOP, fill=tk.X, pady=10)

    def _close(self):
        self._parent.focus_set()
        self.destroy()

    def ok(self):
        self._close()

    def cancel(self):
        self._canceled = True
        self._close()

    def value(self):
        if self._canceled:
            return None

        newVal = self._editorVar.get()

        if self._isEnum:
            newVal = self._enumRawValue(newVal)

        if newVal == self._value:
            return None

        return newVal

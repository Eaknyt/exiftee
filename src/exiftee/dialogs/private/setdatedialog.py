# -*- coding: utf-8 -*-

import datetime
import tkinter as tk
import tkinter.ttk as ttk

import exiftee.tk


class _SetDateDialog(tk.Toplevel):
    def __init__(self, parent, dateTime):
        tk.Toplevel.__init__(self, parent)

        self._parent = parent
        self._currentDateTime = dateTime

        self.title(_('Date editor'))

        self._setupUI()

        self.protocol('WM_DELETE_WINDOW', self.cancel)
        self._canceled = False

        self.grab_set()

        parent.wait_window(self)

    def _setupUI(self):
        self._modeVar = tk.IntVar()

        # Absolute date widgets
        absDateModeFrame = tk.Frame(self)
        absDateModeFrame.grid(row=0, column=0)

        absDateRadio = tk.Radiobutton(absDateModeFrame,
                                      text=_('Use an absolute date'),
                                      command=self._onModeChanged,
                                      variable=self._modeVar,
                                      value=0)

        absDateRadio.grid(row=0, column=0, sticky=tk.W + tk.E)

        absDateWidgetsFrame = tk.Frame(absDateModeFrame)
        absDateWidgetsFrame.grid(row=1, column=0, padx=10, pady=20)

        absDateLabel = tk.Label(absDateWidgetsFrame,
                                text=_('Date (Year, Month, Day) : '))
        absDateLabel.grid(row=0, column=0)

        self._dateEditor = exiftee.tk.DateEntry(absDateWidgetsFrame)
        self._dateEditor.grid(row=0, column=1)

        absTimeLabel = tk.Label(absDateWidgetsFrame,
                                text=_('Time (Hours, Minutes, Seconds) : '))
        absTimeLabel.grid(row=1, column=0)

        absTimeFrame = tk.Frame(absDateWidgetsFrame)
        absTimeFrame.grid(row=1, column=1)

        self._dateHoursVar = tk.StringVar()
        self._dateHoursSbox = tk.Spinbox(absTimeFrame,
                                         from_=0, to=24, increment=1,
                                         textvariable=self._dateHoursVar)
        self._dateHoursSbox.grid(row=0, column=1)

        self._dateMinsVar = tk.StringVar()
        self._dateMinsSbox = tk.Spinbox(absTimeFrame,
                                        from_=0, to=60, increment=1,
                                        textvariable=self._dateMinsVar)
        self._dateMinsSbox.grid(row=0, column=2)

        self._dateSecondsVar = tk.StringVar()
        self._dateSecondsSbox = tk.Spinbox(absTimeFrame,
                                           from_=0, to=60, increment=1,
                                           textvariable=self._dateSecondsVar)
        self._dateSecondsSbox.grid(row=0, column=3)

        # Shift widgets
        shiftModeFrame = tk.Frame(self)
        shiftModeFrame.grid(row=1, column=0)

        shiftRadio = tk.Radiobutton(shiftModeFrame,
                                    text=_('Shift the dates'),
                                    command=self._onModeChanged,
                                    variable=self._modeVar,
                                    value=1)
        shiftRadio.grid(row=0, column=0, sticky=tk.W + tk.E)

        shiftWidgetsFrame = tk.Frame(shiftModeFrame)
        shiftWidgetsFrame.grid(row=1, column=0, padx=10, pady=20)

        shiftDateLabel = tk.Label(shiftWidgetsFrame,
                                  text=_('Date (Year, Month, Day) : '))
        shiftDateLabel.grid(row=0, column=0)

        self._shiftYearsVar = tk.StringVar()
        self._shiftYearsSbox = tk.Spinbox(shiftWidgetsFrame,
                                          from_=0, to=200, increment=1,
                                          textvariable=self._shiftYearsVar)
        self._shiftYearsSbox.grid(row=0, column=1)

        self._shiftMonthsVar = tk.StringVar()
        self._shiftMonthsSbox = tk.Spinbox(shiftWidgetsFrame,
                                           from_=0, to=12, increment=1,
                                           textvariable=self._shiftMonthsVar)
        self._shiftMonthsSbox.grid(row=0, column=2)

        self._shiftDaysVar = tk.StringVar()
        self._shiftDaysSbox = tk.Spinbox(shiftWidgetsFrame,
                                         from_=0, to=31, increment=1,
                                         textvariable=self._shiftDaysVar)
        self._shiftDaysSbox.grid(row=0, column=3)

        shiftTimeLabel = tk.Label(shiftWidgetsFrame,
                                  text=_('Time (Hours, Minutes, Seconds) : '))
        shiftTimeLabel.grid(row=1, column=0)

        self._shiftHoursVar = tk.StringVar()
        self._shiftHoursSbox = tk.Spinbox(shiftWidgetsFrame,
                                          from_=0, to=24, increment=1,
                                          textvariable=self._shiftHoursVar)
        self._shiftHoursSbox.grid(row=1, column=1)

        self._shiftMinsVar = tk.StringVar()
        self._shiftMinsSbox = tk.Spinbox(shiftWidgetsFrame,
                                         from_=0, to=60, increment=1,
                                         textvariable=self._shiftMinsVar)
        self._shiftMinsSbox.grid(row=1, column=2)

        self._shiftSecondsVar = tk.StringVar()
        self._shiftSecondsSbox = tk.Spinbox(shiftWidgetsFrame,
                                            from_=0, to=60, increment=1,
                                            textvariable=self._shiftSecondsVar)
        self._shiftSecondsSbox.grid(row=1, column=3)

        self._shiftFwdVar = tk.IntVar()
        self._shiftFowardCheckbox = tk.Checkbutton(shiftWidgetsFrame,
                                                   text=_('Shift forward'),
                                                   variable=self._shiftFwdVar)
        self._shiftFowardCheckbox.grid(row=1, column=4)

        self._onModeChanged()

        # Create the buttons
        btnsFrame = tk.Frame(self)

        applyBtn = ttk.Button(btnsFrame, text=_('Apply'),
                              command=self.ok)
        applyBtn.pack(side=tk.LEFT, expand=True)

        cancelBtn = ttk.Button(btnsFrame, text=_('Cancel'),
                               command=self.cancel)
        cancelBtn.pack(side=tk.LEFT, expand=True)

        btnsFrame.grid(row=2, pady=10)

    def _onModeChanged(self):
        if self._modeVar.get() == 0:
            self._dateEditor.setState(state=tk.NORMAL)
            self._dateHoursSbox.config(state=tk.NORMAL)
            self._dateMinsSbox.config(state=tk.NORMAL)
            self._dateSecondsSbox.config(state=tk.NORMAL)

            self._shiftYearsSbox.config(state=tk.DISABLED)
            self._shiftMonthsSbox.config(state=tk.DISABLED)
            self._shiftDaysSbox.config(state=tk.DISABLED)
            self._shiftHoursSbox.config(state=tk.DISABLED)
            self._shiftMinsSbox.config(state=tk.DISABLED)
            self._shiftSecondsSbox.config(state=tk.DISABLED)
            self._shiftFowardCheckbox.config(state=tk.DISABLED)
        else:
            self._dateEditor.setState(state=tk.DISABLED)
            self._dateHoursSbox.config(state=tk.DISABLED)
            self._dateMinsSbox.config(state=tk.DISABLED)
            self._dateSecondsSbox.config(state=tk.DISABLED)

            self._shiftYearsSbox.config(state=tk.NORMAL)
            self._shiftMonthsSbox.config(state=tk.NORMAL)
            self._shiftDaysSbox.config(state=tk.NORMAL)
            self._shiftHoursSbox.config(state=tk.NORMAL)
            self._shiftMinsSbox.config(state=tk.NORMAL)
            self._shiftSecondsSbox.config(state=tk.NORMAL)
            self._shiftFowardCheckbox.config(state=tk.NORMAL)

    def _close(self):
        self._parent.focus_set()
        self.destroy()

    def ok(self):
        self._close()

    def okPurge(self):
        self._purgeMetadata = True
        self._close()

    def cancel(self):
        self._canceled = True
        self._close()

    def dateTime(self):
        """
            If the dialog mode is to "Apply a new date", returns a date time
            with the datas of the dialog. Otherwise returns a tuple of two
            elements : the first is the type of shift (forward, backward),
            the second is a string formatted to the exiftool date format.
        """
        if self._canceled:
            return None

        if self._modeVar.get() == 0:
            ret = datetime.datetime(self._dateEditor.date.year,
                                    self._dateEditor.date.month,
                                    self._dateEditor.date.day,
                                    int(self._dateHoursVar.get()),
                                    int(self._dateMinsVar.get()),
                                    int(self._dateSecondsVar.get()))
        else:
            pattern = '{0}:{1}:{2} {3}:{4}:{5}'

            shiftFormatted = pattern.format(self._shiftYearsVar.get(),
                                            self._shiftMonthsVar.get(),
                                            self._shiftDaysVar.get(),
                                            self._shiftHoursVar.get(),
                                            self._shiftMinsVar.get(),
                                            self._shiftSecondsVar.get())

            shiftMode = '-'
            if self._shiftFwdVar.get() == 1:
                shiftMode = '+'

            ret = (shiftMode, shiftFormatted)

        return ret

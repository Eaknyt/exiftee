# -*- coding: utf-8 -*-

import tkinter as tk
import tkinter.messagebox
import tkinter.ttk as ttk

import exiftee.exifviews


class _DeleteTagDialog(tk.Toplevel):
    def __init__(self, parent, files):
        tk.Toplevel.__init__(self, parent)

        self._parent = parent
        self.title(_('Delete Metadata'))

        self._setupUI(files)

        self.protocol('WM_DELETE_WINDOW', self.cancel)
        self._canceled = False

        self._purgeMetadata = False

        self.grab_set()

        parent.wait_window(self)

    def _setupUI(self, files):
        # Create the instructions label
        self.helpLabel = tk.Label(self, text=_('Double click on a tag below ' \
                                               'to add it to the tags to ' \
                                               'delete.'))
        self.helpLabel.pack(side=tk.TOP, fill=tk.X, pady=10)

        # Setup the tag name view
        self._deleteTagView = exiftee.exifviews.DeleteTagView(self)
        self._deleteTagView.files = files

        self._deleteTagView.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        # Create the buttons
        btnsFrame = tk.Frame(self)

        deleteTagsBtn = ttk.Button(btnsFrame, text=_('Delete selected tags'),
                                   command=self.ok)
        deleteTagsBtn.pack(side=tk.LEFT, expand=True)

        purgeTagsBtn = ttk.Button(btnsFrame, text=_('Delete ALL tags'),
                                  command=self._showWarnMessage)
        purgeTagsBtn.pack(side=tk.LEFT, expand=True)

        cancelBtn = ttk.Button(btnsFrame, text=_('Cancel'),
                               command=self.cancel)
        cancelBtn.pack(side=tk.LEFT, expand=True)

        btnsFrame.pack(side=tk.TOP, fill=tk.X, pady=10)

    def _showWarnMessage(self):
        message = _('This action will remove ALL metadatas from the ' \
                    'selected photos.\n\nIt is NOT undoable.\n' \
                    'Are you sure to continue ?')

        if tk.messagebox.askokcancel(_('Purge metadatas'), message,
                                     parent=self,
                                     default=tk.messagebox.CANCEL):
            self.okPurge()

    def _close(self):
        self._parent.focus_set()
        self.destroy()

    def ok(self):
        self._close()

    def okPurge(self):
        self._purgeMetadata = True
        self._close()

    def cancel(self):
        self._canceled = True
        self._close()

    def tags(self):
        if self._canceled:
            return []
        elif self._purgeMetadata:
            return 'all'
        else:
            return self._deleteTagView.checkedTags()

# -*- coding: utf-8 -*-

"""
    The ``exiftee.dialogs.private`` package
    =======================================

"""

from exiftee.dialogs.private.deletetagdialog import _DeleteTagDialog
from exiftee.dialogs.private.setdatedialog import _SetDateDialog
from exiftee.dialogs.private.renamedialog import _RenameDialog
from exiftee.dialogs.private.edittagdialog import _EditTagDialog

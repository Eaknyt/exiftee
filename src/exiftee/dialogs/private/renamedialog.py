# -*- coding: utf-8 -*-

import tkinter as tk
import tkinter.ttk as ttk

import exiftee.exifviews


class _RenameDialog(tk.Toplevel):
    def __init__(self, parent, files):
        tk.Toplevel.__init__(self, parent)

        self._parent = parent
        self._theSettings = parent._settings

        self.title(_('Rename pictures'))

        self._currentPattern = ''

        self._setupUI(files)

        self.protocol('WM_DELETE_WINDOW', self.cancel)
        self._canceled = False

        self.grab_set()

        parent.wait_window(self)

    def _setupUI(self, files):
        # Create the instructions label
        helpText = _('Compose a name pattern below. ' \
                     'You can insert variables using the right panel.')

        helpLabel = tk.Label(self, text=helpText)
        helpLabel.pack(side=tk.TOP, fill=tk.X, pady=10)

        mainFrame = tk.Frame(self)
        mainFrame.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        # Setup the text widget
        patternFrame = tk.Frame(mainFrame)
        patternFrame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        patternHelpText = _('Name pattern')
        patternAreaLabel = tk.Label(patternFrame, text=patternHelpText,
                                    anchor=tk.W, justify=tk.LEFT)
        patternAreaLabel.pack(side=tk.TOP, fill=tk.X)

        self._textEdit = tk.Text(patternFrame, wrap=tk.WORD)

        self._textEdit.tag_config('tag', background='cyan', relief=tk.GROOVE,
                                  borderwidth=1)
        self._textEdit.tag_config('var', background='red', relief=tk.GROOVE,
                                  borderwidth=1)

        self._textEdit.bind('<Key-BackSpace>', self._onBSpaceTriggerred)
        self._textEdit.bind('<Key-Delete>', self._onDelTriggerred)
        self._textEdit.bind('<ButtonRelease-1>', self._checkCurrentCursor)
        self._textEdit.bind('<KeyRelease-Left>', self._onLeftReleased)
        self._textEdit.bind('<KeyRelease-Up>', self._checkCurrentCursor)
        self._textEdit.bind('<KeyRelease-Right>', self._onRightReleased)
        self._textEdit.bind('<KeyRelease-Down>', self._checkCurrentCursor)
        self._textEdit.bind('<KeyRelease-Return>', self._onEnterReleased)

        self._textEdit.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        # Setup the tag name view
        variablesFrame = tk.Frame(mainFrame)
        variablesFrame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        self._varsNoteBook = ttk.Notebook(variablesFrame)
        self._varsNoteBook.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        self._tagNameView = exiftee.exifviews.TagNameView(self._varsNoteBook,
                                                          self._theSettings)
        self._tagNameView.files = files

        self._varsNoteBook.add(self._tagNameView, text=_('EXIF Tags'))

        # Setup the variables view
        self._variablesView = tk.Listbox(self._varsNoteBook)
        self._variablesList = ['%d', '%f', '%e']

        self._variablesView.insert(tk.END, _('Current directory'))
        self._variablesView.insert(tk.END, _('Current file name'))
        self._variablesView.insert(tk.END, _('File extension'))

        self._variablesView.selection_set(0)

        self._varsNoteBook.add(self._variablesView, text=_('Variables'))

        insertVarBtn = tk.Button(variablesFrame, text=_('Insert as text'),
                                 command=self._onInsertTriggerred)
        insertVarBtn.pack(side=tk.TOP, fill=tk.X)

        # Create the buttons
        btnsFrame = tk.Frame(self)

        deleteTagsBtn = ttk.Button(btnsFrame, text=_('Rename'),
                                   command=self.ok)
        deleteTagsBtn.pack(side=tk.LEFT, expand=True)

        cancelBtn = ttk.Button(btnsFrame, text=_('Cancel'),
                               command=self.cancel)
        cancelBtn.pack(side=tk.LEFT, expand=True)

        btnsFrame.pack(side=tk.TOP, fill=tk.X, pady=10)

    def _onInsertTriggerred(self):
        currIndex = self._varsNoteBook.index(self._varsNoteBook.select())

        text = ''

        # Pick the selected tag from the tag view
        if currIndex == 0:
            currentItemText = self._tagNameView.currentText()

            text = '${' + currentItemText + '}'
            tagToUse = 'tag'

            if text == '$':
                text = ''

        # Or pick the selected variable from the variable view
        elif currIndex == 1:
            currentItemIndex = self._variablesView.curselection()[0]

            text = self._variablesList[currentItemIndex]
            tagToUse = 'var'

        if text:
            self._textEdit.insert(tk.INSERT, text, tagToUse)

    def _onBSpaceTriggerred(self, event=None):
        prevTagRange = self._textEdit.tag_prevrange('tag', tk.INSERT,
                                                    'insert linestart')
        prevVarRange = self._textEdit.tag_prevrange('var', tk.INSERT,
                                                    'insert linestart')

        currIndex = self._textEdit.index(tk.INSERT)

        if prevTagRange and prevTagRange[1] == currIndex:
            self._textEdit.delete(prevTagRange[0], prevTagRange[1])
        elif prevVarRange and prevVarRange[1] == currIndex:
            self._textEdit.delete(prevVarRange[0], prevVarRange[1])

    def _onDelTriggerred(self, event=None):
        nextTagRange = self._textEdit.tag_nextrange('tag', tk.INSERT,
                                                    'insert lineend')
        nextVarRange = self._textEdit.tag_nextrange('var', tk.INSERT,
                                                    'insert lineend')

        currIndex = self._textEdit.index(tk.INSERT)

        if nextTagRange and nextTagRange[0] == currIndex:
            self._textEdit.delete(nextTagRange[0], nextTagRange[1])
        elif nextVarRange and nextVarRange[0] == currIndex:
            self._textEdit.delete(nextVarRange[0], nextVarRange[1])

    def _indexIsWithin(self, index, idxRange):
        """
            Returns True if the index 'index' is contained within the index
            range 'idxRange', otherwise returns False.
        """
        return (self._textEdit.compare(idxRange[0], '<', index) and
                self._textEdit.compare(index, '<', idxRange[1]))

    def _checkTagHere(self, tagName, end=True):
        currIndex = self._textEdit.index(tk.INSERT)

        try:
            prevRange = self._textEdit.tag_prevrange(tagName, tk.INSERT,
                                                     'tag.first')

            if (prevRange):
                if self._indexIsWithin(currIndex, prevRange):
                    self._textEdit.mark_set(tk.INSERT, prevRange[int(end)])
        except Exception:
            pass

    def _checkCurrentCursor(self, event=None):
        self._checkTagHere('tag')
        self._checkTagHere('var')

    def _onRightReleased(self, event=None):
        self._checkCurrentCursor()

    def _onEnterReleased(self, event=None):
        self._textEdit.delete(tk.INSERT)

    def _onLeftReleased(self, event=None):
        self._checkTagHere('tag', False)
        self._checkTagHere('var', False)

    def _close(self):
        self._parent.focus_set()
        self.destroy()

    def ok(self):
        # (auto add an implicit counter and the file extension)
        self._currentPattern = self._textEdit.get('1.0', 'end-1c') + '_%c.%e'
        self._close()

    def cancel(self):
        self._canceled = True
        self._close()

    def pattern(self):
        return self._currentPattern

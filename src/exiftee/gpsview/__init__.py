# -*- coding: utf-8 -*-

"""
    The ``exiftee.gpsview`` package
    =================================

"""

from exiftee.gpsview.gpsview import GpsView
import exiftee.gpsview.locations

# -*- coding: utf-8 -*-

import os.path

import PIL.Image
import PIL.ImageTk

import tkinter as tk

import exiftee
import exiftee.gpsview.locations


class GpsView(tk.Frame):
    """
        The ``GpsView`` provides a view to focus on geographic coordinates
        within a given area.

        The ``corners`` property defines this area, and the ``coords`` property
        defines the current coordinates.

        The current coordinates are shown as a red circle, called the
        indicator. You can hide or show this indicator using the
        ``indicatorVisible`` property.

        The GpsView handles GPS coordinates as decimals.
    """
    def __init__(self, parent=None, **kwargs):
        tk.Frame.__init__(self, parent, **kwargs)

        self._corners = ((0, 0), (0, 0))
        self._coords = None

        self._indicatorSize = 10
        self._indicatorVisible = True

        self._bgSize = (400, 400)

        self._canvas = tk.Canvas(self, width=self._bgSize[0],
                                 height=self._bgSize[1])

        self._cache = []

        self._initCanvas()

    def _initCanvas(self):
        """
            Creates the canvas elements, including the background image and
            the indicator.
        """
        #TODO instanciate the France image dynamically, e.g. if the current
        #  bounding box is the bb of France

        self._canvas.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

        franceMapRelPath = 'src/exiftee/gpsview/resources/france_map.jpg'

        franceMapImg = PIL.Image.open(os.path.join(exiftee.appPath(),
                                                   franceMapRelPath))
        franceMapImg.thumbnail(self._bgSize, PIL.Image.ANTIALIAS)

        canvasBg = PIL.ImageTk.PhotoImage(franceMapImg)

        self._cache.append(canvasBg)

        # Create the background map
        self._canvas.create_image(self._canvas.winfo_reqwidth() / 2,
                                  self._canvas.winfo_reqheight() / 2,
                                  image=canvasBg, anchor=tk.CENTER)

        # Create the indicator
        x0 = self._indicatorSize - self._indicatorSize
        y0 = self._indicatorSize - self._indicatorSize
        x1 = self._indicatorSize + self._indicatorSize
        y1 = self._indicatorSize + self._indicatorSize

        self._indicatorItem = self._canvas.create_oval(x0, y0, x1, y1,
                                                       fill='red')
        self.indicatorVisible = False

    def _areWithinBoundingBox(self):
        """
            Returns true if the current coordinates are located in the current
            bounding box (which is defined by the corners property), otherwise
            returns false.
        """
        if not self._coords:
            return False

        lat = self._coords[0]
        longt = self._coords[1]

        topLeft = self._corners[0]
        bottomRight = self._corners[1]

        if lat > topLeft[0] or lat < bottomRight[0]:
            return False

        if longt < topLeft[1] or longt > bottomRight[1]:
            return False

        return True

    def _coordsToPixels(self):
        """
            Converts the current coordinates, which are expressed in GPS
            coordinates, to canvas coordinates.
        """
        lat = self._coords[0]
        lon = self._coords[1]

        latMin = self._corners[0][0]
        latMax = self._corners[1][0]
        lonMin = self._corners[0][1]
        lonMax = self._corners[1][1]

        x = self._canvas.winfo_width() * (lon - lonMin) / (lonMax - lonMin)
        y = self._canvas.winfo_height() * (lat - latMin) / (latMax - latMin)

        return (x, y)

    def _pixelsToCoords(self, pos):
        """
            Converts the pixel coordinates 'pos' to gps coordinates.
        """
        pxX = pos[0]
        pxY = pos[1]

        latMin = self._corners[0][0]
        latMax = self._corners[1][0]
        lonMin = self._corners[0][1]
        lonMax = self._corners[1][1]

        lat = pxY / (self._canvas.winfo_height() / (latMax - latMin)) + latMin
        lon = pxX / (self._canvas.winfo_width() / (lonMax - lonMin)) + lonMin

        return (lat, lon)

    def _updateIndicatorState(self):
        if self._indicatorVisible:
            newState = tk.NORMAL
        else:
            newState = tk.HIDDEN

        self._canvas.itemconfigure(self._indicatorItem, state=newState)

    def _refreshCanvas(self):
        self.indicatorVisible = self._areWithinBoundingBox()

        if self.indicatorVisible:
            pos = self._coordsToPixels()
            oldPos = self._canvas.coords(self._indicatorItem)

            x = pos[0] - oldPos[2] + self._indicatorSize
            y = pos[1] - oldPos[3] + self._indicatorSize

            self._canvas.move(self._indicatorItem, x, y)

    @property
    def corners(self):
        """
            The corners property defines the bounding box of the area displayed
            by the gps view.

            Corners must be a tuple of two tuples, each containing two floats.

            The first sub-tuple defines the top left corner, the other defines
            the bottom right.

            Unlike a (x, y) plane, the latitude (the "y" coordinate) is
            declared first.

            The ``exiftee.gpsview.location`` module provides the bounding boxes
            of famous locations::

                import exiftee.gpsview

                myGpsView = exiftee.gpsview.GpsView()
                myGpsView.corners = exiftee.gpsview.locations.FRANCE
        """
        return self._corners

    @corners.setter
    def corners(self, newCorners):
        if self._corners != newCorners:
            self._corners = newCorners

    @property
    def coords(self):
        """
            The coords property defines the position currently handled by the
            gps view.

            A red indicator is displayed at the current coordinates.
        """
        return self._coords

    @coords.setter
    def coords(self, newCoords):
        if self._coords != newCoords:
            self._coords = newCoords

            self._refreshCanvas()

    @property
    def indicatorVisible(self):
        """
            The indicatorVisible property defines the state of the current
            coordinates indicator.

            Pass False to this property setter to hide the indicator, or pass
            True to show it.
        """
        return self._indicatorVisible

    @indicatorVisible.setter
    def indicatorVisible(self, visible):
        if self._indicatorVisible != visible:
            self._indicatorVisible = visible

            self._updateIndicatorState()

    def clear(self):
        """
            Reset the current coordinates and hide the indicator.
        """
        self.coords = None

    def update(self):
        self._refreshCanvas()

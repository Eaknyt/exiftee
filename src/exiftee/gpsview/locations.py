# -*- coding: utf-8 -*-

"""
    The ``exiftee.gpsview.locations`` module aims to provide known coordinates,
    like the bounding box coordinates of various country.

    See : http://boundingbox.klokantech.com/
"""

FRANCE = ((51.08, -5.01), (41.3, 9.55))

# -*- coding: utf-8 -*-

"""
    The ``exiftee`` package contains the sources related to the Exiftee
    software.
"""

import mimetypes
import os


def appPath():
    return os.getcwd()


def localPath():
    return os.path.join(os.path.expanduser('~'), '.exiftee')


def helpPath():
    return os.path.join(appPath(), 'help')


def i18nPath():
    return os.path.join(os.getcwd(), 'share/locale')


def supportedMimeTypes():
    return ['image/jpeg', 'image/png', 'image/bmp', 'image/x-windows-bmp',
            'image/x-ms-bmp', 'image/tiff', 'image/x-tiff', 'image/png']


def fileIsSupported(path):
    types = supportedMimeTypes()

    fileMimeType = mimetypes.guess_type(path)

    return fileMimeType[0] in types


def supportedTagCategories():
    """
        http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/

            todo:: Add all other categories
    """
    return ['EXIF', 'IPTC', 'XMP', 'ICC_Profile', 'Photoshop', 'JPEG']

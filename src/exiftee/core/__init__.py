# -*- coding: utf-8 -*-

"""
    The ``exiftee.core`` package
    ============================

    The ``exiftee.core`` package contains the foundations of Exiftee.
"""

from exiftee.core.actionmanager import Action
from exiftee.core.actionmanager import ActionManager
from exiftee.core.mainwindow import MainWindow

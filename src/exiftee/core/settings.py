# -*- coding: utf-8 -*-

import configparser
import os.path

import exiftee


class Settings(configparser.ConfigParser):
    """
        The ``Settings`` class manages the Exiftee configuration.

        Reading and writing the configuration file
        ------------------------------------------

        Use ``load()`` to retrieve the settings stored on disk, and ``save()``
        to write the actual settings to the disk.

        Use ``restoreDefaults()`` followed by ``save()`` to restore the
        original default configuration.
    """
    def __init__(self):
        configparser.ConfigParser.__init__(self)

        # Declare key names constants
        self.OPEN_WELCOME_ID = 'open_welcome'
        self.MODE_ID = 'mode'
        self.FAVS_ID = 'favorites_folders'
        self.TAG_CATS_ID = 'tag_categories'
        self.SHORTCUTS_ID = 'shortcuts'

        # Create attributes
        self.restoreDefaults()

        # Create the settings file if not exists
        p = self.userSettingsPath()

        if os.path.exists(p):
            self.load()
        else:
            self.restoreDefaults()
            self.save()

    def userSettingsPath(self):
        return os.path.join(exiftee.localPath(), 'user.ini')

    def defaultSettingsPath(self):
        return os.path.join(exiftee.localPath(), 'default.ini')

    def modes(self):
        return ['Basic', 'Advanced']

    def _deserializeFavorites(self):
        serialized = self['DEFAULT'][self.FAVS_ID]

        if serialized:
            lines = serialized.split('\n')

            self.favoritesFolders = [fav for fav in lines
                                     if os.path.exists(fav)]

            if lines != self.favoritesFolders:
                self._serializeFavorites()
                self.save()

    def _serializeFavorites(self):
        lines = ''

        for fav in self.favoritesFolders:
            lines += fav
            lines += '\n'

        lines = lines[:-1]

        self['DEFAULT'][self.FAVS_ID] = lines

    def _deserializeTagCategories(self):
        serialized = self['DEFAULT'][self.TAG_CATS_ID]

        if serialized:
            cats = serialized.split(', ')
            supportedCats = exiftee.supportedTagCategories()

            self.tagCategories = [cat for cat in cats if cat in supportedCats]

            if cats != self.tagCategories:
                self._serializeTagCategories()
                self.save()

    def _serializeTagCategories(self):
        supportedCats = exiftee.supportedTagCategories()

        if not set(self.tagCategories).issubset(supportedCats):
            raise ValueError('Cannot serialize : the tag category is invalid.')

        self['DEFAULT'][self.TAG_CATS_ID] = ', '.join(supportedCats)

    def _deserializeShortcuts(self):
        serialized = self['DEFAULT'][self.SHORTCUTS_ID]

        if serialized:
            scuts = serialized.split('\n')

            #TODO check that each shortcut is valid
            for s in scuts:
                tup = s.split(': ')

                if len(tup) != 2:
                    raise ValueError('Invalid shortcut')

                (actionId, shortcut) = tup

                if not shortcut:
                    raise ValueError('Invalid shortcut')

                self.customShortcuts[actionId] = shortcut

    def _serializeShortcuts(self):
        lines = ''

        for (actionId, shortcut) in self.customShortcuts.items():
            lines += '{0}: {1}'.format(actionId, shortcut)
            lines += '\n'

        lines = lines[:-1]

        self['DEFAULT'][self.SHORTCUTS_ID] = lines

    def _deserialize(self):
        self.openWelcome = self['DEFAULT'].getboolean(self.OPEN_WELCOME_ID)
        self.mode = self['DEFAULT'][self.MODE_ID]
        self._deserializeFavorites()
        self._deserializeTagCategories()
        self._deserializeShortcuts()

    def _serialize(self):
        self['DEFAULT'][self.OPEN_WELCOME_ID] = str(int(self.openWelcome))
        self['DEFAULT'][self.MODE_ID] = self.mode
        self._serializeFavorites()
        self._serializeTagCategories()
        self._serializeShortcuts()

    def load(self):
        self.read(self.userSettingsPath())

        self._deserialize()

    def save(self):
        self._serialize()

        with open(self.userSettingsPath(), 'w') as configFile:
            self.write(configFile)

    def restoreDefaults(self):
        self.openWelcome = True
        self.mode = self.modes()[0]
        self.favoritesFolders = []
        self.tagCategories = ['EXIF']
        self.customShortcuts = {}

    def modeCategories(self):
        if self.mode == 'Basic':
            ret = ['EXIF']
        else:
            ret = exiftee.supportedTagCategories()

        return ret

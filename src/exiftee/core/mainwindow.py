# -*- coding: utf-8 -*-

# Builtin packages
import PIL
import tkinter as tk
import tkinter.filedialog
import tkinter.messagebox
import tkinter.ttk as ttk
import webbrowser

# PySide packages
from PySide.QtGui import QUndoStack

# Own packages
import exiftee.core
import exiftee.core.settings
import exiftee.dialogs
import exiftee.exiftoolext
import exiftee.exifviews
import exiftee.explorers
import exiftee.gpsview
import exiftee.photoviewer
import exiftee.tk
import exiftee.undo


class MainWindow(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.title('exiftee')

        self._undoStack = QUndoStack()

        self._settings = exiftee.core.settings.Settings()

        self._initConstants()
        self._setupActions()
        self._setupUI()
        self._setupBindings()

        self._currentDir = ''
        self.currentDir = self._dirView.rootPath

        self._restoreUserSettings()

    # Initialization methods

    def _initConstants(self):
        """
            Create the constants of the application. It's mainly constants that
            will be used by the Action Manager.

            In this case, declare your constants as below :
            <contextPrefix>_CTX = '...'
            <actionPrefix>_ID = '...'

            (context first, actions next)
        """
        # Global context
        self.APP_CTX = 'Exiftee'

        self.ADD_FAV_ID = 'AddFavoriteFolder'
        self.SELECT_ALL_ID = 'SelectAllFiles'
        self.DEL_TAG_DIALOG_ID = 'OpenDeleteTagDialog'
        self.SET_DATE_DIALOG_ID = 'OpenSetDateDialog'
        self.RENAME_DIALOG_ID = 'OpenRenameDialog'
        self.OPEN_SETTINGS_ID = 'OpenSettingsDialog'
        self.UNDO_ID = 'Undo'
        self.REDO_ID = 'Redo'
        self.OPEN_HELP_ID = 'OpenHelp'
        self.OPEN_WELCOME_ID = 'OpenWelcome'
        self.OPEN_ABOUT_ID = 'OpenAbout'

    def _setupActions(self):
        """
            Register the Exiftee actions in the Action Manager.
        """
        self.am = exiftee.core.ActionManager(self)

        self.am.registerAction(self.ADD_FAV_ID, self.APP_CTX,
                               _('Add a favorite folder'),
                               self._onAddFavTriggerred, '<Control-d>')

        self.am.registerAction(self.SELECT_ALL_ID, self.APP_CTX,
                               _('Select all files'),
                               self._onSelectAllFilesTrigerred, '<Control-a>')

        self.am.registerAction(self.DEL_TAG_DIALOG_ID, self.APP_CTX,
                               _('Delete Metadata'),
                               self._onDeleteTagToolTriggerred)

        self.am.registerAction(self.SET_DATE_DIALOG_ID, self.APP_CTX,
                               _('Modify date'),
                               self._onSetDateToolTriggerred)

        self.am.registerAction(self.RENAME_DIALOG_ID, self.APP_CTX,
                               _('Rename pictures'),
                               self._onRenameToolTriggerred)

        self.am.registerAction(self.OPEN_SETTINGS_ID, self.APP_CTX,
                               _('Preferences'),
                               self._onSettingsTriggerred, '<Control-p>')

        self.am.registerAction(self.UNDO_ID, self.APP_CTX,
                               _('Undo'),
                               self._onUndoTriggerred, '<Control-z>')

        self.am.registerAction(self.REDO_ID, self.APP_CTX,
                               _('Redo'),
                               self._onRedoTriggerred, '<Control-Shift-Z>')

        self.am.registerAction(self.OPEN_HELP_ID, self.APP_CTX,
                               _('Open help'),
                               self._onOpenHelpTriggerred, '<F1>')

        self.am.registerAction(self.OPEN_WELCOME_ID, self.APP_CTX,
                               _('Welcome'),
                               self._onOpenWelcomeTriggerred)

        self.am.registerAction(self.OPEN_ABOUT_ID, self.APP_CTX,
                               _('About'),
                               self._onOpenAboutTriggerred)

    def _setupBindings(self):
        """
            Register the shortcuts and the callbacks for each action registered
            in the Action Manager.

            Remember that the context attribute passed to each Action object
            MUST identify a widget in the tkinter name engine, except for the
            'Exiftee' string, which identify the application's main window.
        """
        for action in self.am.actions.values():
            if action.keySeq and action.callback:
                if action.context == self.APP_CTX:
                    self.bind_all(action.keySeq, action.callback)
                else:
                    ctxWidget = self.nametowidget(action.context)
                    ctxWidget.bind(action.keySeq, action.callback)

    def rebind(self, actionId, oldKeySeq, newKeySeq):
        action = self.am.actions[actionId]

        if action.callback:
            if action.context == self.APP_CTX:
                self.unbind_all(oldKeySeq)

                if newKeySeq:
                    self.bind_all(newKeySeq, action.callback)
            else:
                ctxWidget = self.nametowidget(action.context)
                ctxWidget.unbind(oldKeySeq, action.callback)

                if newKeySeq:
                    ctxWidget.bind(newKeySeq, action.callback)

    def _setupMenuBar(self):
        """
            Create the main window's menu bar and add the most important
            actions to it.
        """
        menuFrame = tk.Frame(self)

        # File menu
        fileMenuBtn = tk.Menubutton(menuFrame, text=_('File'), underline=0)
        fileMenu = tk.Menu(fileMenuBtn)

        #  File.AddFavoriteFolder action
        self.am.addCommandToMenu(fileMenu, self.am.actions[self.ADD_FAV_ID])

        fileMenuBtn.config(menu=fileMenu)
        fileMenuBtn.pack(side=tk.LEFT)

        # Edit menu
        editMenuBtn = tk.Menubutton(menuFrame, text=_('Edit'), underline=0)
        self._editMenu = tk.Menu(editMenuBtn)

        #  Edit.Undo action
        self.am.addCommandToMenu(self._editMenu, self.am.actions[self.UNDO_ID])

        #  Edit.Redo action
        self.am.addCommandToMenu(self._editMenu, self.am.actions[self.REDO_ID])

        editMenuBtn.config(menu=self._editMenu)
        editMenuBtn.pack(side=tk.LEFT)

        # View menu
        viewMenuBtn = tk.Menubutton(menuFrame, text=_('View'), underline=0)
        viewMenu = tk.Menu(viewMenuBtn)

        #  View.SelectAllFiles action
        self.am.addCommandToMenu(viewMenu, self.am.actions[self.SELECT_ALL_ID])

        viewMenuBtn.config(menu=viewMenu)
        viewMenuBtn.pack(side=tk.LEFT)

        # Tools menu
        toolsMenuBtn = tk.Menubutton(menuFrame, text=_('Tools'), underline=0)
        toolsMenu = tk.Menu(toolsMenuBtn)

        toolsMenuBtn.config(menu=toolsMenu)
        toolsMenuBtn.pack(side=tk.LEFT)

        #  Tools.OpenDeleteTagDialog action
        self.am.addCommandToMenu(toolsMenu,
                                 self.am.actions[self.DEL_TAG_DIALOG_ID])

        menuFrame.pack(side=tk.TOP, fill=tk.X)

        # Tools.OpenSetDateDialog action
        self.am.addCommandToMenu(toolsMenu,
                                 self.am.actions[self.SET_DATE_DIALOG_ID])

        #  Tools.OpenRenameDialog action
        self.am.addCommandToMenu(toolsMenu,
                                 self.am.actions[self.RENAME_DIALOG_ID])

        #  Tools.OpenSettingsDialog action
        self.am.addSeperatorToMenu(toolsMenu)

        self.am.addCommandToMenu(toolsMenu,
                                 self.am.actions[self.OPEN_SETTINGS_ID])

        # Grays out the undo and redo menu actions
        self._updateUndoMenus()

        # Help menu
        helpMenuBtn = tk.Menubutton(menuFrame, text=_('Help'), underline=0)
        helpMenu = tk.Menu(helpMenuBtn)

        helpMenuBtn.config(menu=helpMenu)
        helpMenuBtn.pack(side=tk.LEFT)

        #  Help.OpenHelp action
        self.am.addCommandToMenu(helpMenu,
                                 self.am.actions[self.OPEN_HELP_ID])

        #  Help.OpenWelcome action
        self.am.addCommandToMenu(helpMenu,
                                 self.am.actions[self.OPEN_WELCOME_ID])

        #  Help.OpenAbout action
        self.am.addCommandToMenu(helpMenu,
                                 self.am.actions[self.OPEN_ABOUT_ID])

    def _setupUI(self):
        self._setupMenuBar()

        self._pathLine = exiftee.tk.PathLine(self)
        self._pathLine.pack(side=tk.TOP, fill=tk.X)

        # Central splitter
        centralSplitter = ttk.Panedwindow(self, orient=tk.HORIZONTAL)
        centralSplitter.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        # Explorer and Favorites
        dirExplorerBook = ttk.Notebook(self)

        self._dirView = exiftee.explorers.DirectoryView(dirExplorerBook)

        dirExplorerBook.add(self._dirView, text=_('Folders'))

        favsBookFrame = tk.Frame(dirExplorerBook)
        favsBookFrame.rowconfigure(1, weight=1)
        favsBookFrame.columnconfigure(0, weight=1)
        favsBookFrame.columnconfigure(1, weight=1)

        addFavBtn = tk.Button(favsBookFrame, text=_('Add'),
                              command=self._onAddFavTriggerred)
        addFavBtn.grid(row=0, column=0, sticky='ew')

        delFavBtn = tk.Button(favsBookFrame, text=_('Delete'),
                              command=self._onDelFavTriggerred)
        delFavBtn.grid(row=0, column=1, sticky='ew')

        self._favsView = exiftee.explorers.FavoritesView(favsBookFrame)
        self._favsView.grid(row=1, column=0, columnspan=2, sticky='nsew')

        dirExplorerBook.add(favsBookFrame, text=_('Favorites'), padding=2,
                            sticky='nsew')

        centralSplitter.add(dirExplorerBook)

        # Photo view
        photoViewFrame = tk.Frame(self)
        photoViewBook = ttk.Notebook(photoViewFrame)

        sortPhotoViewFrame = tk.Frame(photoViewFrame)

        #  Sort tag chooser
        sortPhotoViewLabel = tk.Label(sortPhotoViewFrame,
                                      text=_('Sort photos'))
        sortPhotoViewLabel.pack(side=tk.LEFT)

        self._sortPhotoViewCBox = ttk.Combobox(sortPhotoViewFrame)
        self._sortPhotoViewCBox.state(['!disabled', 'readonly'])
        self._sortPhotoViewCBox.bind('<<ComboboxSelected>>',
                                     self._onSortCriteriaChanged)
        self._sortPhotoViewCBox.pack(side=tk.LEFT, fill=tk.X, expand=True)

        #  Sort order chooser
        sortOrderLabel = tk.Label(sortPhotoViewFrame,
                                  text=_('Ascending order'))
        sortOrderLabel.pack(side=tk.LEFT)

        self._sortOrderVar = tk.IntVar()
        sortOrderCheckBox = tk.Checkbutton(sortPhotoViewFrame,
                                           command=self._onSortOrderChanged,
                                           variable=self._sortOrderVar)

        sortOrderCheckBox.pack(side=tk.LEFT)

        sortPhotoViewFrame.pack(side=tk.TOP, fill=tk.X)

        self._photoView = exiftee.photoviewer.PhotoView(photoViewBook)
        photoViewBook.add(self._photoView, text=_('Images'))
        photoViewBook.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        centralSplitter.add(photoViewFrame)

        # Previewer, tag view and gps view
        rightPaneSplitter = ttk.Panedwindow(self, orient=tk.VERTICAL)

        previewerBook = ttk.Notebook(rightPaneSplitter)

        self._previewer = tk.Label(previewerBook,
                                   text=_('No image is selected'))
        previewerBook.add(self._previewer, text=_('Preview'))

        self._previewCache = None

        rightPaneSplitter.add(previewerBook)

        tagViewBook = ttk.Notebook(rightPaneSplitter)

        self._tagView = exiftee.exifviews.TagView(tagViewBook, self._settings)
        tagViewBook.add(self._tagView, text=_('Metadata'))

        rightPaneSplitter.add(tagViewBook)

        gpsViewFoldable = exiftee.tk.Foldable(rightPaneSplitter)
        gpsViewFoldable.text = _('GPS View')

        self._gpsView = exiftee.gpsview.GpsView(gpsViewFoldable.placeholder)
        self._gpsView.corners = exiftee.gpsview.locations.FRANCE
        self._gpsView.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        rightPaneSplitter.add(gpsViewFoldable)

        # Well well well, here it's truly amazing.
        # It seems that the entire API of ttk.Panedwindow.sash_* functions for
        #  Python 3.4 is JUST BROKEN. So you can't resize the splitter
        #  programmatically.
        #  Welcome, poor UI experience.

        centralSplitter.add(rightPaneSplitter)

        # Inter-widgets communication
        self._dirView.command = self._onDirChanged_internal
        self._favsView.command = self._onDirChanged_internal
        self._pathLine.command = self._onDirChanged_internal

        self._photoView.callbacks['pathChanged'] = self._onDirChanged_internal
        self._photoView.callbacks['selectionChanged'] = self._onSelectionChanged

        self._tagView.callbacks['tagDoubleClicked'] = self._onTagViewDClicked

    def _restoreUserSettings(self):
        """
            Sets the Exiftee configuration to its last state.
        """
        # Retrieve favorites folders
        self._favsView.paths = self._settings.favoritesFolders

        # Retrieve custom shortcuts
        for (actionId, shortcut) in self._settings.customShortcuts.items():
            if actionId in self.am.actions:
                keySeq = self.am.fromReadableShortcut(shortcut)

                self.am.setKeySeq(actionId, keySeq)

        # Open the Welcome dialog if necessary
        if self._settings.openWelcome:
            self._onOpenWelcomeTriggerred()

    def _updateSortCriterias(self):
        imagePaths = self._photoView.content()

        cats = self._settings.tagCategories

        tagNames = exiftee.exiftoolext.getTagNamesByCat(cats, imagePaths,
                                                        False)

        self._sortPhotoViewCBox.config(values=tagNames)

    def _updateUndoMenus(self):
        enabledStates = [tk.NORMAL, tk.ACTIVE]

        # Check the undo menu entry
        UNDO_ENTRY_IDX = 1
        updatedState = None
        currentUndoState = self._editMenu.entrycget(UNDO_ENTRY_IDX,
                                                    'state')

        if (currentUndoState in enabledStates and not
           self._undoStack.canUndo()):
            updatedState = tk.DISABLED
        elif currentUndoState == tk.DISABLED and self._undoStack.canUndo():
            updatedState = tk.NORMAL

        self._editMenu.entryconfigure(UNDO_ENTRY_IDX, state=updatedState)

        # Check the undo menu entry
        REDO_ENTRY_IDX = 2
        updatedState = None
        currentRedoState = self._editMenu.entrycget(REDO_ENTRY_IDX,
                                                    'state')

        if (currentRedoState in enabledStates and not
           self._undoStack.canRedo()):
            updatedState = tk.DISABLED
        elif currentRedoState == tk.DISABLED and self._undoStack.canRedo():
            updatedState = tk.NORMAL

        self._editMenu.entryconfigure(REDO_ENTRY_IDX, state=updatedState)

    def _refreshGpsView(self):
        gpsCoords = self._tagView.getGpsCoords()

        if gpsCoords:
            self._gpsView.coords = gpsCoords
        else:
            self._gpsView.clear()

    def _pushUndoCommand(self, cmd):
        self._undoStack.push(cmd)

        self._updateUndoRelatedWidgets()

    def _updateUndoRelatedWidgets(self):
        self._tagView.update()

    # Interaction methods

    def _onAddFavTriggerred(self, event=None):
        newFav = tk.filedialog.askdirectory(parent=self,
                                            title=_('Add a favorite folder'))

        self.addFavoriteDir(newFav)

    def _onDelFavTriggerred(self, event=None):
        fav = self._favsView.current()

        if not fav:
            errorMsg = _('No favorite is selected.')
            tk.messagebox.showerror(title=_('Delete favorite'),
                                    message=errorMsg)

        self.removeFavoriteDir(fav)

    def _onSelectAllFilesTrigerred(self, event=None):
        self._photoView.selectAll()

    def _onSortCriteriaChanged(self, event=None):
        sortTag = self._sortPhotoViewCBox.get()

        self._photoView.setSortTag(sortTag)

    def _onSortOrderChanged(self, event=None):
        self._photoView.reverseSortOrder()

    def _onDeleteTagToolTriggerred(self, event=None):
        selectedPaths = self._photoView.getSelection()

        if not selectedPaths:
            errorMsg = _('You must select some files to use this tool.')
            tk.messagebox.showerror(title=_('Error'), message=errorMsg)
            return

        tagNames = exiftee.dialogs.askTagsToDelete(self, selectedPaths)

        if not tagNames:
            return

        if tagNames == 'all':
            exiftee.exiftoolext.purgeTags(selectedPaths)
            self._tagView.update()
        else:
            undoData = {}
            values = exiftee.exiftoolext.getTags(selectedPaths, tagNames)

            for (fileAndTagName, val) in values.items():
                undoData[fileAndTagName] = (val, '')

            cmd = exiftee.undo.DeleteTagCommand(undoData)
            self._pushUndoCommand(cmd)

        self._updateUndoMenus()

    def _onSetDateToolTriggerred(self, event=None):
        selectedPaths = self._photoView.getSelection()

        if not selectedPaths:
            errorMsg = _('You must select some files to use this tool.')
            tk.messagebox.showerror(title=_('Error'), message=errorMsg)
            return

        dialogData = exiftee.dialogs.askDate(self)

        if not dialogData:
            return

        if type(dialogData) is tuple:
            shiftMode = dialogData[0]
            toApply = dialogData[1]
        else:
            shiftMode = ''
            toApply = dialogData

        cmd = exiftee.undo.SetDateCommand(toApply, selectedPaths, shiftMode)

        self._pushUndoCommand(cmd)

        self._updateUndoMenus()

    def _onRenameToolTriggerred(self, event=None):
        selectedPaths = self._photoView.getSelection()

        if not selectedPaths:
            errorMsg = _('You must select some files to use this tool.')
            tk.messagebox.showerror(title=_('Error'), message=errorMsg)
            return

        pattern = exiftee.dialogs.askRenamingPattern(self, selectedPaths)

        if not pattern:
            return

        exiftee.exiftoolext.rename(pattern, selectedPaths)

        self._photoView.update()

    def _onSettingsTriggerred(self, event=None):
        exiftee.dialogs.SettingsDialog(self)

    def _onUndoTriggerred(self, event=None):
        self._undoStack.undo()

        self._updateUndoRelatedWidgets()
        self._updateUndoMenus()

    def _onRedoTriggerred(self, event=None):
        self._undoStack.redo()

        self._updateUndoRelatedWidgets()
        self._updateUndoMenus()

    def _onOpenHelpTriggerred(self, event=None):
        webbrowser.open(exiftee.helpPath())

    def _onOpenWelcomeTriggerred(self, event=None):
        exiftee.dialogs.WelcomeDialog(self)

    def _onOpenAboutTriggerred(self, event=None):
        exiftee.dialogs.AboutDialog(self)

    def _onDirChanged_internal(self, newDir):
        self.currentDir = newDir

    def _onSelectionChanged(self):
        self._tagView.files = self._photoView.getSelection()

        if not self._tagView.files:
            self._previewCache = None
            self._previewer.config(image='', text=_('No image is selected'))

            self._gpsView.clear()
        else:
            # Refresh the previewer
            img = PIL.Image.open(self._tagView.files[0])
            img.thumbnail((300, 200), PIL.Image.ANTIALIAS)

            self._previewCache = PIL.ImageTk.PhotoImage(img)
            self._previewer.config(image=self._previewCache)

            # Refresh the gps view
            self._refreshGpsView()

    def _onTagViewDClicked(self, tag, value):
        newVal = exiftee.dialogs.askTagValue(self, tag, value)

        if not newVal:
            return

        currentFile = self._tagView.currentFile()

        undoData = {
            (currentFile, tag): (value, newVal)
        }

        cmd = exiftee.undo.SetTagCommand(undoData)

        self._pushUndoCommand(cmd)

        self._updateUndoMenus()

        if tag in exiftee.exiftoolext.gpsTagNames():
            self._refreshGpsView()

    def addFavoriteDir(self, path):
        self._favsView.addPath(path)

        self._settings.favoritesFolders = self._favsView.paths
        self._settings.save()

    def removeFavoriteDir(self, path):
        self._favsView.delPath(path)

        self._settings.favoritesFolders = self._favsView.paths
        self._settings.save()

    def setMode(self, mode):
        if mode not in self._settings.modes():
            raise ValueError('Mode {0} does not exist'.format(mode))

        # Reload the tag view
        self._tagView.update()

    # Properties

    @property
    def currentDir(self):
        return self._currentDir

    @currentDir.setter
    def currentDir(self, newDir):
        if self._currentDir != newDir:
            self._currentDir = newDir

            self._photoView.path = newDir
            self._pathLine.path = newDir

            self._updateSortCriterias()

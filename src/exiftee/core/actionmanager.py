# -*- coding: utf-8 -*-


class Action(object):
    """
        The ``Action`` class abstracts a user action.

        You should never instanciate an such object by yourself. Use
        ``ActionManager`` instead.

        An ``Action`` is described by the following :
        - ``context`` is a string that describes the context in which the
        action will be triggerred. A context can be shared across multiple
        ``Action``.
        - ``text`` describes the action in a few words. Keep in mind that it
        may be displayed in a menu entry.
        - ``keySeq`` is the key sequence used to trigger the action. If your
        action does not provide a shortcut, just leave it empty. Otherwise it
        MUST follow the tkinter rules, without the angle brackets.
        For detail about these rules :
        http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/event-types.html
        http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/event-modifiers.html
        - ``callback`` is a Python function that will be executed.
        It can be null.
    """
    def __init__(self):
        self.context = ''
        self.text = ''
        self.keySeq = ''
        self.callback = None
        self.menu = None
        self.menuIdx = -1


class ActionManager(object):
    """
        The ``ActionManager`` class handles a set of ``Action``.

        Key sequences and shortcuts
        ---------------------------
        The ``ActionManager`` deals with "key sequences" and "shortcuts".

        - A key sequence is a string formatted to match the format of tkinter
        accelerators. Example : '<Control-x>'

        - A shortcut is a string formatted to be user-readable. Alpha keys are
        uppercase. Example : 'Ctrl+X'

        Use ``readableShortcut()`` to get a shortcut string from a key
        sequence. Alternatively, use ``fromReadableShortcut()`` to get a key
        sequence string from a shortcut.

            .. seealso:: readableShortcut()
            .. seealso:: fromReadableShortcut()
    """
    def __init__(self, app=None):
        self._app = app

        self.actions = {}

    def registerAction(self, actionId, ctx, text, callback=None, keySeq=''):
        """
            Registers an action identified by ``actionId``.
            For more detail about ``Action`` objects, see its documentation.
        """
        a = Action()
        a.context = ctx
        a.text = text
        a.keySeq = keySeq
        a.callback = callback

        self.actions[actionId] = a

        return a

    def setCallback(self, actionId, callback):
        """
            Sets the callback associated with ``actionId`` to ``callback``.
        """
        a = self.actions[actionId]
        a.callback = callback

    def setKeySeq(self, actionId, keySeq):
        """
            Sets the shortcut associated with ``actionId`` to ``keySeq``.
        """
        a = self.actions[actionId]

        oldKeySeq = a.keySeq

        a.keySeq = keySeq

        self._app.rebind(actionId, oldKeySeq, keySeq)

        self._updateMenu(actionId)

    def trigger(self, actionId):
        """
            Executes the callback of the ``Action`` associated with
            ``actionId``.
        """
        a = self.actions[actionId]

        if a.callback:
            a.callback()

    def readableShortcut(self, keySeq):
        """
            Returns a user readable string containing the shortcut from
            ``keySeq``, which is a tkinter-formatted key sequence.

            This function assumes that ``keySeq`` is like :
            <[modifier-]key>
        """
        if not keySeq:
            return keySeq

        ret = keySeq

        ret = ret.replace('<', '').replace('>', '').replace('-', '+')

        # Replace "Control" by "Ctrl" if necessary
        ctrlIdx = ret.find('Control')

        if ctrlIdx != -1:
            ret = ret.replace('Control', 'Ctrl')

        # Uppercase the last char
        ret = ret[:-1] + ret[-1].upper()

        return ret

    def fromReadableShortcut(self, shortcut):
        """
            Returns a tkinter key sequence from the string 'shortcut'.

            This function assumes that 'shortcut' was created with
            ``readableShortcut()``.

                .. seealso:: readableShortcut()
        """
        if not shortcut:
            return shortcut

        ret = shortcut.replace('+', '-')

        # Replace "Ctrl" by "Control" if necessary
        ctrlIdx = ret.find('Ctrl')

        if ctrlIdx != -1:
            ret = ret.replace('Ctrl', 'Control')

        # Lowercase the last char
        ret = ret[:-1] + ret[-1].lower()
        ret = '<' + ret + '>'

        return ret

    def addCommandToMenu(self, menu, action):
        if not action.menu:
            action.menu = menu

            if not menu.index('last'):
                nextIdx = 1
            else:
                nextIdx = menu.index('last') + 1

            action.menuIdx = nextIdx

        menu.add_command(label=action.text,
                         accelerator=self.readableShortcut(action.keySeq),
                         command=action.callback)

    def addSeperatorToMenu(self, menu):
        menu.add_separator()

    def _updateMenu(self, actionId):
        action = self.actions[actionId]

        if not action.menu:
            raise ValueError('Action must be associated with a menu.')

        menu = action.menu

        newText = action.text
        newShortcut = self.readableShortcut(action.keySeq)

        currentText = menu.entrycget(action.menuIdx, 'label')
        currentShortcut = menu.entrycget(action.menuIdx, 'accelerator')

        if newText != currentText:
            menu.entryconfig(action.menuIdx, label=newText)

        if newShortcut != currentShortcut:
            menu.entryconfig(action.menuIdx, accelerator=newShortcut)

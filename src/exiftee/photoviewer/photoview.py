# -*- coding: utf-8 -*-

import os
import os.path
import tkinter as tk

import PIL.Image
import PIL.ImageTk

import exiftee
import exiftee.exiftoolext


class PhotoView(tk.Frame):
    """
        The ``PhotoView`` displays the EXIFable files and folders contained
        in a path.

        The user can navigate into folders, and perform a selection on some
        images.

        The ``path`` property can be used to modify the current path.
        The ``prefSize`` property can be used to modify the size of displayed
        items.
    """
    def __init__(self, parent=None, **kwargs):
        tk.Frame.__init__(self, parent, **kwargs)

        self._path = ''
        self._sortTag = ''
        self._sortOrder = True

        # Item cache
        self._items = []
        self._cache = []
        self._contentToDraw = []

        # Constants (for item tags)
        self._ITEM_BG_TAG = 'itemBg'
        self._ITEM_IMG_TAG = 'itemImg'
        self._ITEM_LABEL_TAG = 'itemLabel'
        self._ITEM_SELECT_TAG = 'selected'

        # Prefered size of items
        self._prefSize = (200, 100)

        # Retrieve custom icons from the resources directory
        resDir = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                              'resources')

        fIcon = PIL.Image.open(os.path.join(resDir, 'folder.png'))
        fIcon.thumbnail(self._prefSize, PIL.Image.ANTIALIAS)

        self._folderImg = PIL.ImageTk.PhotoImage(fIcon)

        # Callbacks
        self.callbacks = {
            'pathChanged': None,
            'selectionChanged': None
        }

        # Build the UI
        self._setupUI()

    def _setupUI(self):
        # Create the canvas
        self._canvas = tk.Canvas(self)
        self._canvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        # Create the canvas scrollbars
        yScrollBar = tk.Scrollbar(self._canvas, orient=tk.VERTICAL)
        yScrollBar.pack(side=tk.RIGHT, fill=tk.Y)

        self._canvas.config(yscrollcommand=yScrollBar.set)
        yScrollBar.config(command=self._canvas.yview)

        xScrollBar = tk.Scrollbar(self._canvas, orient=tk.HORIZONTAL)
        xScrollBar.pack(side=tk.BOTTOM, fill=tk.X)

        self._canvas.config(xscrollcommand=xScrollBar.set)
        xScrollBar.config(command=self._canvas.xview)

        # Bind the canvas events
        self._canvas.bind('<Button-1>', self._onLeftClicked)
        self._canvas.bind('<Double-Button-1>', self._onDoubleClicked)

    def _truncatedName(self, fileName):
        extIndex = fileName.rfind('.')
        extension = fileName[extIndex:]

        ret = fileName[:14] + '...' + extension

        return ret

    def _clearCache(self):
        self._cache.clear()

    def _clearScene(self):
        self._items.clear()
        self._canvas.delete(tk.ALL)

    def _drawItem(self, x, y, path, sortedValue=None):
        # Draw the background rectangle
        itemRect = [x - self._prefSize[0] / 2,
                    y - self._prefSize[1] / 2,
                    x + self._prefSize[0] / 2,
                    y + self._prefSize[1] / 2]

        bgItem = self._canvas.create_rectangle(itemRect[0], itemRect[1],
                                               itemRect[2], itemRect[3],
                                               width=0,
                                               tags=[path, self._ITEM_BG_TAG])

        self._items.append(bgItem)

        # Draw the image
        canvImg = None

        if not os.path.isdir(path):
            img = PIL.Image.open(path)
            img.thumbnail(self._prefSize, PIL.Image.ANTIALIAS)

            canvImg = PIL.ImageTk.PhotoImage(img)
            self._cache.append(canvImg)
        else:
            canvImg = self._folderImg

        self._canvas.create_image(x, y, image=canvImg,
                                  tags=[path, self._ITEM_IMG_TAG])

        # Draw the entry text
        fileName = path.split(os.sep)[-1]
        label = fileName

        if len(fileName) > 20:
            label = self._truncatedName(fileName)

        self._canvas.create_text(x, y + self._prefSize[1] / 1.7, text=label,
                                 tags=[path, self._ITEM_LABEL_TAG])

    def _retrieveSortedContent(self):
        self._contentToDraw = exiftee.exiftoolext.sort(self._sortTag,
                                                       self.content(),
                                                       self._sortOrder)

    def _refreshCanvas(self):
        self._clearScene()

        x = 0
        y = 0

        for (entry, sortValue) in self._contentToDraw:
            self._drawItem(x, y, entry)

            x += (self._prefSize[0] + 20)

            if x >= self._canvas.winfo_width():
                y += (self._prefSize[1] * 1.5)
                x = 0

        self._canvas.config(scrollregion=self._canvas.bbox(tk.ALL))

    def _findBgItem(self, path):
        bgItems = self._canvas.find_withtag(self._ITEM_BG_TAG)

        for item in bgItems:
            if path in self._canvas.gettags(item):
                return item

        return None

    def _isSelected(self, rectItem):
        return self._ITEM_SELECT_TAG in self._canvas.gettags(rectItem)

    def _selectItem(self, rectItem, sel):
        # Check if the item is already selected or not
        isSelected = self._isSelected(rectItem)

        if (isSelected and sel) or (not isSelected and not sel):
            return

        # Un/Select the item
        bdWidth = 0
        fillColor = ''
        stippleMode = ''

        if sel:
            bdWidth = 1
            fillColor = 'cyan'
            stippleMode = 'gray25'
            self._canvas.addtag_withtag(self._ITEM_SELECT_TAG, rectItem)
        else:
            self._canvas.dtag(rectItem, self._ITEM_SELECT_TAG)

        self._canvas.itemconfig(rectItem, width=bdWidth,
                                fill=fillColor, stipple=stippleMode)

    def _selectedItems(self):
        return self._canvas.find_withtag(self._ITEM_SELECT_TAG)

    def _clearSelection(self):
        for item in self._selectedItems():
            self._canvas.dtag(item, self._ITEM_SELECT_TAG)
            self._canvas.itemconfig(item, fill='', stipple='', width=0)

    def _onLeftClicked(self, event):
        # Focus the canvas
        self._canvas.focus_set()

        # Retrieve the current selection
        currentSel = self._selectedItems()

        # Check if the click occured in the canvas background
        item = self._canvas.find_withtag('current')

        if not item:
            self._clearSelection()
        else:
            # Retrieve the rectangle item for the selection process
            bgItem = None

            tags = self._canvas.gettags(item)

            if self._ITEM_BG_TAG not in tags:
                path = tags[0]
                bgItem = self._findBgItem(path)
            else:
                bgItem = item[0]

            if not bgItem:
                return

            # Perform a selection with the clicked item
            s = event.state

            # Ctrl is pressed
            if s & 0x0004:
                self._selectItem(bgItem, not self._isSelected(bgItem))
            else:
                self._clearSelection()
                self._selectItem(bgItem, True)

        # Check if the selection has changed
        if currentSel != self._selectedItems():
            self.callbacks['selectionChanged']()

    def _onDoubleClicked(self, event):
        item = self._canvas.find_withtag('current')

        if item:
            itemPath = self._canvas.gettags(item)[0]

            if os.path.isdir(itemPath):
                self.path = itemPath

    def content(self):
        """
            Returns a list containing the paths of folders and supported images
            contained in the PhotoView's current path.
        """
        ret = []

        for entry in os.listdir(self._path):
            absPath = os.path.join(self._path, entry)

            if os.path.isdir(absPath) or exiftee.fileIsSupported(entry):
                ret.append(absPath)

        return ret

    def getSelection(self):
        """
            Returns a list of the paths of selected items.
        """
        ret = [self._canvas.gettags(item)[0] for item in self._selectedItems()]

        return ret

    @property
    def path(self):
        """
            The path property defines the path used by the PhotoView to display
            its images and folders.
        """
        return self._path

    @path.setter
    def path(self, newPath):
        if self._path != newPath:
            self._path = newPath

            self._clearCache()

            self.update()

            if (self.callbacks['pathChanged']):
                self.callbacks['pathChanged'](newPath)

    @path.deleter
    def path(self):
        self._path = ''

    @property
    def prefSize(self):
        """
            The prefSize property defines the size of the displayed items.
        """
        return self._prefSize

    @prefSize.setter
    def prefSize(self, newSize):
        if self._prefSize != newSize:
            self._prefSize = newSize

            self._refreshCanvas()

    @prefSize.deleter
    def prefSize(self):
        self._prefSize = (200, 100)

    # API

    def back(self):
        """
            Set the current path to the parent folder.
        """
        joined = os.path.join(self._path, '..')
        self.path = os.path.normpath(joined)

    def setSortTag(self, tagName):
        if self._sortTag != tagName:
            self._sortTag = tagName

            self._retrieveSortedContent()

            self._refreshCanvas()

    def reverseSortOrder(self):
        self._sortOrder = not self._sortOrder
        self._contentToDraw.reverse()

        self._refreshCanvas()

    def update(self):
        self._retrieveSortedContent()

        self._refreshCanvas()

    def selectAll(self):
        currentSel = self._selectedItems()

        for item in self._items:
            self._selectItem(item, True)

        # Check if the selection has changed
        if currentSel != self._selectedItems():
            self.callbacks['selectionChanged']()

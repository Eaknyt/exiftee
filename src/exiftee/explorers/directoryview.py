# -*- coding: utf-8 -*-

import os
import tkinter as tk
import tkinter.ttk as ttk


class DirectoryView(tk.Frame):
    """
        The ``DirectoryView`` class provides a tree view acting like a
        directory browser.

        The items of the tree are the children directories of the ``rootPath``
        property. The tree is not recursively filled. When the user expands a
        node, the items that matches this directory's children are added to
        this node.
    """
    def __init__(self, parent=None, **kwargs):
        tk.Frame.__init__(self, parent, kwargs)

        self._rootPath = os.path.expanduser('~')
        self._command = None

        self._setupUI()
        self._refresh()

    def _setupUI(self):
        # Create the tree view
        self._tree = ttk.Treeview(self, selectmode=tk.BROWSE, show='tree')
        self._tree.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        # Add scrollbars to the tree view
        yScrollBar = ttk.Scrollbar(self._tree, orient=tk.VERTICAL)
        yScrollBar.pack(side=tk.RIGHT, fill=tk.Y)

        self._tree.config(yscrollcommand=yScrollBar.set)
        yScrollBar.config(command=self._tree.yview)

        xScrollBar = ttk.Scrollbar(self, orient=tk.HORIZONTAL)
        xScrollBar.pack(side=tk.BOTTOM, fill=tk.X)

        self._tree.config(xscrollcommand=xScrollBar.set)
        xScrollBar.config(command=self._tree.xview)

        # Bind the tree events
        self._tree.bind('<<TreeviewOpen>>', self._onItemExpanded)
        self._tree.bind('<Double-Button-1>', self._onItemDoubleClicked)

    def _subdirs(self, path):
        """
            Returns the names of the subdirectories of ``path``.
        """
        ret = []

        entries = os.listdir(path)

        for entry in entries:
            absPath = os.path.join(path, entry)

            if os.path.isdir(absPath) and not entry[0] == '.':
                ret.append(entry)

        return sorted(ret)

    def _itemPath(self, iid):
        """
            Returns the full path of the item defined by ``iid``.
        """
        it = iid
        ancestors = []

        while True:
            if not it:
                break

            pText = self._tree.item(it, 'text')
            ancestors.insert(0, pText)

            it = self._tree.parent(it)

        ret = os.path.join(self.rootPath, *ancestors)

        return ret

    def _parseSubdirs(self, path, item, level=1):
        """
            Add the subdir items to ``item``, which corresponds to ``path``.

            ``level`` is the recursion count.
        """
        children = self._tree.get_children(item)

        if not children:
            for entry in self._subdirs(path):
                iid = self._tree.insert(item, 'end', text=entry, open=False)

                if level > 1:
                    entryPath = os.path.join(path, entry)
                    self._parseSubdirs(entryPath, iid, level - 1)
        else:
            for c in children:
                entryPath = os.path.join(path, self._tree.item(c, 'text'))

                if level > 1:
                    self._parseSubdirs(entryPath, c, level - 1)

    def _refresh(self):
        """
            Repopulate the tree view using the ``DirectoryView`` 's rootpath.

            The rootpath and the Unix hidden directories are not displayed.
            All newly added items are collapsed.
        """
        # Clear the tree view
        self._tree.delete(*self._tree.get_children())

        # Populate the items on two levels
        self._parseSubdirs(self.rootPath, '', 2)

    def _onItemExpanded(self, event):
        """
            Parse the subdirs associated with the just expanded item.
        """
        # (1) The TreeviewExpand/Collapse virtual events are amazingly
        # USELESS, because the event coordinates are f***** invalid, so we
        # can't use the ttk.Treeview.identify() function to retrieve the
        # expanded item. Well.
        # (2) What's wrong with this API ? You can modify the current item
        # selection, but you can't get the current selection. Anyway.
        # Let's use this ugly workaround :
        iid = self._tree.focus()

        # Get the full path of the clicked item, iterating other its ancestors
        path = self._itemPath(iid)

        # Insert children
        # TODO don't call the function below if there's no dir
        self._parseSubdirs(path, iid, 2)

    def _onItemDoubleClicked(self, event):
        if self._command:
            self._command(self.current())

    @property
    def rootPath(self):
        """
            The rootPath property describes the root item of the view.

            The tree view uses it to display all children directories.
        """
        return self._rootPath

    @rootPath.setter
    def rootPath(self, newPath):
        if self.rootPath != newPath:
            self.rootPath = newPath

            self._refresh()

    @rootPath.deleter
    def rootPath(self):
        self._rootPath = os.path.expanduser('~')

    @property
    def command(self):
        """
            The command property defines a callback which is called when a
            folder item is double-clicked in the DirectoryView.

            The path of the clicked item will be submitted as the first
            argument of the callback.
        """
        return self._command

    @command.setter
    def command(self, cmd):
        if self._command != cmd:
            self._command = cmd

    @command.deleter
    def command(self):
        self._command = None

    def current(self):
        """
            Returns the path associated with the current selected item.
        """
        selectedItem = self._tree.focus()

        return self._itemPath(selectedItem)

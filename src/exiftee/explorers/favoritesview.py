# -*- coding: utf-8 -*-

import os
import tkinter as tk
import tkinter.ttk as ttk


class FavoritesView(tk.Frame):
    """
        The ``FavoritesView`` provides a list box of favorites folders.

        For each folder, the view knows its full path on the disk.

        Note : since we can't associate additional data to the items of a
        tk.Listbox, we use a ttk.Treeview to represent an interactive list
        widget.
    """
    def __init__(self, parent=None, **kwargs):
        tk.Frame.__init__(self, parent, kwargs)

        self._tree = ttk.Treeview(self, selectmode=tk.BROWSE, show='tree')
        self._tree.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        self._paths = []
        self._command = None

        # Add scrollbars to the list box
        yScrollBar = ttk.Scrollbar(self._tree, orient=tk.VERTICAL)
        yScrollBar.pack(side=tk.RIGHT, fill=tk.Y)

        self._tree.config(yscrollcommand=yScrollBar.set)
        yScrollBar.config(command=self._tree.yview)

        xScrollBar = ttk.Scrollbar(self._tree, orient=tk.HORIZONTAL)
        xScrollBar.pack(side=tk.BOTTOM, fill=tk.X)

        self._tree.config(xscrollcommand=xScrollBar.set)
        xScrollBar.config(command=self._tree.xview)

        # Bind the tree events
        self._tree.bind('<Double-Button-1>', self._onItemDoubleClicked)

    def _addFolderItem(self, path):
        folderName = path.split(os.sep)[-1]

        self._tree.insert('', 'end', text=folderName, tags=[path])

    def _removeFolderItem(self, path):
        idToRemove = self._tree.tag_has(path)

        self._tree.delete(idToRemove)

    def _onItemDoubleClicked(self, event):
        clickedItem = self.current()

        if self._command and clickedItem:
            self._command(clickedItem)

    @property
    def paths(self):
        """
            The paths property provides access to the paths currently handled
            by the FavoritesView.
        """
        return self._paths

    @paths.setter
    def paths(self, paths):
        if self._paths != paths:
            self._paths = paths

            # Clear the tree view
            self._tree.delete(*self._tree.get_children())

            # Create and add the items to the tree view
            for p in paths:
                self._addFolderItem(p)

    def addPath(self, path):
        """
            Adds a path to the FavoritesView.
        """
        if path not in self._paths:
            self.paths.append(path)

            self._addFolderItem(path)

    def delPath(self, path):
        if path in self._paths:
            self._paths.remove(path)

            self._removeFolderItem(path)

    @property
    def command(self):
        """
            The command property defines a callback which is called when a
            folder item is double-clicked in the FavoritesView.

            The path of the clicked item will be submitted as the first
            argument of the callback.
        """
        return self._command

    @command.setter
    def command(self, cmd):
        if self._command != cmd:
            self._command = cmd

    @command.deleter
    def command(self):
        self._command = None

    def current(self):
        """
            Returns the path associated with the current selected item.
        """
        selectedItem = self._tree.focus()

        if not selectedItem:
            return None

        tags = self._tree.item(selectedItem, 'tags')

        return tags[0]

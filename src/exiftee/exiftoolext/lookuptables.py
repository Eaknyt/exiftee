# -*- coding: utf-8 -*-

"""
    The ``exiftee.exiftoolext.lookuptables`` module contains the ExifTool
    lookup tables described here :
    http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames

    Only tags from the EXIF category are listed for the moment.

    The lookup tables are required to print human-readable values for several
    tags.

    Since exiftee uses PyExifTool as a wrapper around ExifTool, we need to
    rebuild manually these tables because :

    - PyExifTool starts with the non-conversion option (-n) by default.

    - The ExifTool frontend does not provide an option to get the
    human-readable value of a given tag value.

    - The original tables seem to be accessible accross the Perl API only.
"""

# EXIF TAGS

exifEnumTags = dict()

exifEnumTags['EXIF:SubfileType'] = {
    0x0: 'Full-resolution Image',
    0x1: 'Reduced-resolution image',
    0x2: 'Single page of multi-page image',
    0x3: 'Single page of multi-page reduced-resolution image',
    0x4: 'Transparency mask',
    0x5: 'Transparency mask of reduced-resolution image',
    0x6: 'Transparency mask of multi-page image',
    0x7: 'Transparency mask of reduced-resolution multi-page image',
    0x10001: 'Alternate reduced-resolution image',
    0xffffffff: 'invalid'
}

exifEnumTags['EXIF:Compression'] = {
    1: 'Uncompressed',
    2: 'CCITT 1D',
    3: 'T4/Group 3 Fax',
    4: 'T6/Group 4 Fax',
    5: 'LZW',
    6: 'JPEG (old-style)',
    7: 'JPEG',
    8: 'Adobe Deflate',
    9: 'JBIG B&W',
    10: 'JBIG Color',
    99: 'JPEG',
    262: 'Kodak 262',
    32766: 'Next',
    32767: 'Sony ARW Compressed',
    32769: 'Packed RAW',
    32770: 'Samsung SRW Compressed',
    32771: 'CCIRLEW',
    32772: 'Samsung SRW Compressed 2',
    32773: 'PackBits',
    32809: 'Thunderscan',
    32867: 'Kodak KDC Compressed',
    32895: 'IT8CTPAD',
    32896: 'IT8LW',
    32897: 'IT8MP',
    32898: 'IT8BL',
    32908: 'PixarFilm',
    32909: 'PixarLog',
    32946: 'Deflate',
    32947: 'DCS',
    34661: 'JBIG',
    34616: 'SGILog',
    34677: 'SGILog24',
    34712: 'JPEG 2000',
    34713: 'Nikon NEF Compressed',
    34715: 'JBIG2 TIFF FX',
    34718: 'Microsoft Document Imaging (MDI) Binary Level Codec',
    34719: 'Microsoft Document Imaging (MDI) Progressive Transform Codec',
    34720: 'Microsoft Document Imaging (MDI) Vector',
    34892: 'Lossy JPEG',
    65000: 'Kodak DCR Compressed',
    65535: 'Pentax PEF Compressed'
}

exifEnumTags['EXIF:LightSource'] = {
    0: 'Unknown',
    1: 'Daylight',
    2: 'Fluorescent',
    3: 'Tungsten (Incandescent)',
    4: 'Flash',
    9: 'Fine Weather',
    10: 'Cloudy',
    11: 'Shade',
    12: 'Daylight Fluorescent',
    13: 'Day White Fluorescent',
    14: 'Cool White Fluorescent',
    15: 'White Fluorescent',
    16: 'Warm White Fluorescent',
    17: 'Standard Light A',
    18: 'Standard Light B',
    19: 'Standard Light C',
    20: 'D55',
    21: 'D65',
    22: 'D75',
    23: 'D50',
    24: 'ISO Studio Tungsten',
    255: 'Other'
}

exifEnumTags['EXIF:Flash'] = {
    0x0: 'No Flash',
    0x1: 'Fired',
    0x5: 'Fired, Return not detected',
    0x7: 'Fired, Return detected',
    0x8: 'On, Did not fire',
    0x9: 'On, Fired',
    0xd: 'On, Return not detected',
    0xf: 'On, Return detected',
    0x10: 'Off, Did not fire',
    0x14: 'Off, Did not fire, Return not detected',
    0x18: 'Auto, Did not fire',
    0x19: 'Auto, Fired',
    0x1d: 'Auto, Fired, Return not detected',
    0x1f: 'Auto, Fired, Return detected',
    0x20: 'No flash function',
    0x30: 'Off, No flash function',
    0x41: 'Fired, Red-eye reduction',
    0x45: 'Fired, Red-eye reduction, Return not detected',
    0x47: 'Fired, Red-eye reduction, Return detected',
    0x49: 'On, Red-eye reduction',
    0x4d: 'On, Red-eye reduction, Return not detected',
    0x4f: 'On, Red-eye reduction, Return detected',
    0x50: 'Off, Red-eye reduction',
    0x58: 'Auto, Did not fire, Red-eye reduction',
    0x59: 'Auto, Fired, Red-eye reduction',
    0x5d: 'Auto, Fired, Red-eye reduction, Return not detected',
    0x5f: 'Auto, Fired, Red-eye reduction, Return detected'
}

exifEnumTags['EXIF:PhotometricInterpretation'] = {
    0: 'WhiteIsZero',
    1: 'BlackIsZero',
    2: 'RGB',
    3: 'RGB Palette',
    4: 'Transparency Mask',
    5: 'CMYK',
    6: 'YCbCr',
    8: 'CIELab',
    9: 'ICCLab',
    10: 'ITULab',
    32803: 'Color Filter Array',
    32844: 'Pixar LogL',
    32845: 'Pixar LogLuv',
    32892: 'Linear Raw'
}

exifEnumTags['EXIF:Thresholding'] = {
    1: 'No dithering or halftoning',
    2: 'Ordered dither or halftone ',
    3: 'Randomized dither'
}

exifEnumTags['EXIF:FillOrder'] = {
    1: 'Normal',
    2: 'Reversed'
}

exifEnumTags['EXIF:Orientation'] = {
    1: 'Horizontal (normal)',
    2: 'Mirror horizontal ',
    3: 'Rotate 180 ',
    4: 'Mirror vertical',
    5: 'Mirror horizontal and rotate 270 CW',
    6: 'Rotate 90 CW',
    7: 'Mirror horizontal and rotate 90 CW',
    8: 'Rotate 270 CW'
}

exifEnumTags['EXIF:PlanarConfiguration'] = {
    1: 'Chunky',
    2: 'Planar'
}

exifEnumTags['EXIF:GrayResponseUnit'] = {
    1: '0.1',
    2: '0.001',
    3: '0.0001',
    4: '1e-05',
    5: '1e-06'
}

exifEnumTags['EXIF:ResolutionUnit'] = {
    1: 'None',
    2: 'inches',
    3: 'cm'
}

exifEnumTags['EXIF:Predictor'] = {
    1: 'None',
    2: 'Horizontal differencing'
}

exifEnumTags['EXIF:CleanFaxData'] = {
    0: 'Clean',
    1: 'Regenerated',
    2: 'Unclean'
}

exifEnumTags['EXIF:InkSet'] = {
    1: 'CMYK',
    2: 'Not CMYK'
}

exifEnumTags['EXIF:ExtraSamples'] = {
    0: 'Unspecified',
    1: 'Associated Alpha',
    2: 'Unassociated Alpha'
}

exifEnumTags['EXIF:SampleFormat'] = {
    1: 'Unsigned',
    2: 'Signed',
    3: 'Float',
    4: 'Undefined',
    5: 'Complex int',
    6: 'Complex float'
}

exifEnumTags['EXIF:Indexed'] = {
    0: 'Not indexed',
    1: 'Indexed'
}

exifEnumTags['EXIF:OPIProxy'] = {
    0: 'Higher resolution image does not exist',
    1: 'Higher resolution image exists'
}

exifEnumTags['EXIF:ProfileType'] = {
    0: 'Unspecified',
    1: 'Group 3 FAX'
}

exifEnumTags['EXIF:FaxProfile'] = {
    0: 'Unknown',
    1: 'Minimal B&W lossless, S',
    2: 'Extended B&W lossless, F',
    3: 'Lossless JBIG B&W, J',
    4: 'Lossy color and grayscale, C',
    5: 'Lossless color and grayscale, L',
    6: 'Mixed raster content, M',
    7: 'Profile T',
    255: 'Multi Profiles'
}

exifEnumTags['EXIF:JPEGProc'] = {
    1: 'Baseline',
    14: 'Lossless'
}

exifEnumTags['EXIF:YCbCrSubSampling'] = {
    '1 1': 'YCbCr4:4:4 (1 1)',
    '1 2': 'YCbCr4:4:0 (1 2)',
    '1 4': 'YCbCr4:4:1 (1 4)',
    '2 1': 'YCbCr4:2:2 (2 1)',
    '2 2': 'YCbCr4:2:0 (2 2)',
    '2 4': 'YCbCr4:2:1 (2 4)',
    '4 1': 'YCbCr4:1:1 (4 1)',
    '4 2': 'YCbCr4:1:0 (4 2)'
}

exifEnumTags['EXIF:YCbCrPositioning'] = {
    1: 'Centered',
    2: 'Co-sited'
}

exifEnumTags['EXIF:SonyRawFileType'] = {
    0: 'Sony Uncompressed 14-bit RAW',
    1: 'Sony Uncompressed 12-bit RAW',
    2: 'Sony Compressed RAW',
    3: 'Sony Lossless Compressed RAW'
}

exifEnumTags['EXIF:RasterPadding'] = {
    0: 'Byte',
    1: 'Word',
    2: 'Long Word',
    9: 'Sector',
    10: 'Long Sector'
}

exifEnumTags['EXIF:ImageColorIndicator'] = {
    0: 'Unspecified Image Color',
    1: 'Specified Image Color'
}

exifEnumTags['EXIF:BackgroundColorIndicator'] = {
    0: 'Unspecified Background Color',
    1: 'Specified Background Color'
}

exifEnumTags['EXIF:HCUsage'] = {
    0: 'CT',
    1: 'Line Art',
    2: 'Trap'
}

exifEnumTags['EXIF:ExposureProgram'] = {
    0: 'Not Defined',
    1: 'Manual',
    2: 'Program AE',
    3: 'Aperture-priority AE',
    4: 'Shutter speed priority AE',
    5: 'Creative (Slow speed)',
    6: 'Action (High speed)',
    7: 'Portrait',
    8: 'Landscape',
    9: 'Bulb'
}

exifEnumTags['EXIF:SensitivityType'] = {
    0: 'Unknown',
    1: 'Standard Output Sensitivity',
    2: 'Recommended Exposure Index',
    3: 'ISO Speed',
    4: 'Standard Output Sensitivity and Recommended Exposure Index',
    5: 'Standard Output Sensitivity and ISO Speed',
    6: 'Recommended Exposure Index and ISO Speed',
    7: 'Standard Output Sensitivity, Recommended Exposure Index and ISO Speed'
}

exifEnumTags['EXIF:ComponentsConfiguration'] = {
    0: '-',
    1: 'Y',
    2: 'Cb',
    3: 'Cr',
    4: 'R',
    5: 'G',
    6: 'B'
}

exifEnumTags['EXIF:MeteringMode'] = {
    0: 'Unknown',
    1: 'Average',
    2: 'Center-weighted average',
    3: 'Spot',
    4: 'Multi-spot',
    5: 'Multi-segment',
    6: 'Partial',
    255: 'Other'
}

exifEnumTags['EXIF:FocalPlaneResolutionUnit'] = {
    1: 'None',
    2: 'inches',
    3: 'cm',
    4: 'mm',
    5: 'um'
}

exifEnumTags['EXIF:SecurityClassification'] = {
    'C': 'Confidential',
    'R': 'Restricted',
    'S': 'Secret',
    'T': 'Top Secret',
    'U': 'Unclassified'
}

exifEnumTags['EXIF:SensingMethod'] = {
    1: 'Monochrome area',
    2: 'One-chip color area',
    3: 'Two-chip color area',
    4: 'Three-chip color area',
    5: 'Color sequential area',
    6: 'Monochrome linear',
    7: 'Trilinear',
    8: 'Color sequential linear'
}

exifEnumTags['EXIF:ColorSpace'] = {
    0x1: 'sRGB',
    0x2: 'Adobe sRGB',
    0xfffd: 'Wide Gamut RGB',
    0xfffe: 'ICC Profile',
    0xffff: 'Uncalibrated'
}

exifEnumTags['EXIF:FileSource'] = {
    1: 'Film Scanner',
    2: 'Reflection Print Scanner',
    3: 'Digital Camera',
    '\x03\x00\x00\x00': 'Sigma Digital Camera'
}

exifEnumTags['EXIF:SceneType'] = {
    1: 'Directly photographed'
}

exifEnumTags['EXIF:CustomRendered'] = {
    0: 'Normal',
    1: 'Custom'
}

exifEnumTags['EXIF:ExposureMode'] = {

}

exifEnumTags['EXIF:'] = {
    0: 'Auto',
    1: 'Manual',
    2: 'Auto bracket'
}

exifEnumTags['EXIF:WhiteBalance'] = {
    0: 'Auto',
    1: 'Manual'
}

exifEnumTags['EXIF:SceneCaptureType'] = {
    0: 'Standard',
    1: 'Landscape',
    2: 'Portrait',
    3: 'Night'
}

exifEnumTags['EXIF:GainControl'] = {
    0: 'None',
    1: 'Low gain up',
    2: 'High gain up',
    3: 'Low gain down',
    4: 'High gain down'
}

exifEnumTags['EXIF:Contrast'] = {
    0: 'Normal',
    1: 'Low',
    2: 'High'
}

exifEnumTags['EXIF:Saturation'] = {
    0: 'Normal',
    1: 'Low',
    2: 'High'
}

exifEnumTags['EXIF:Sharpness'] = {
    0: 'Normal',
    1: 'Soft',
    2: 'Hard'
}

exifEnumTags['EXIF:SubjectDistanceRange'] = {
    0: 'Unknown',
    1: 'Macro',
    2: 'Close',
    3: 'Distant'
}

exifEnumTags['EXIF:PixelFormat'] = {
    0x5: 'Black & White',
    0x8: '8-bit Gray',
    0x9: '16-bit BGR555',
    0xa: '16-bit BGR565',
    0xb: '16-bit Gray',
    0xc: '24-bit BGR',
    0xd: '24-bit RGB',
    0xe: '32-bit BGR',
    0xf: '32-bit BGRA',
    0x10: '32-bit PBGRA',
    0x11: '32-bit Gray Float',
    0x12: '48-bit RGB Fixed Point',
    0x13: '32-bit BGR101010',
    0x15: '48-bit RGB',
    0x16: '64-bit RGBA',
    0x17: '64-bit PRGBA',
    0x18: '96-bit RGB Fixed Point',
    0x19: '128-bit RGBA Float',
    0x1a: '128-bit PRGBA Float',
    0x1b: '128-bit RGB Float',
    0x1c: '32-bit CMYK',
    0x1d: '64-bit RGBA Fixed Point',
    0x1e: '128-bit RGBA Fixed Point',
    0x1f: '64-bit CMYK',
    0x20: '24-bit 3 Channels',
    0x21: '32-bit 4 Channels',
    0x22: '40-bit 5 Channels',
    0x23: '48-bit 6 Channels',
    0x24: '56-bit 7 Channels',
    0x25: '64-bit 8 Channels',
    0x26: '48-bit 3 Channels',
    0x27: '64-bit 4 Channels',
    0x28: '80-bit 5 Channels',
    0x29: '96-bit 6 Channels',
    0x2a: '112-bit 7 Channels',
    0x2b: '128-bit 8 Channels',
    0x2c: '40-bit CMYK Alpha',
    0x2d: '80-bit CMYK Alpha',
    0x2e: '32-bit 3 Channels Alpha',
    0x2f: '40-bit 4 Channels Alpha',
    0x30: '48-bit 5 Channels Alpha',
    0x31: '56-bit 6 Channels Alpha',
    0x32: '64-bit 7 Channels Alpha',
    0x33: '72-bit 8 Channels Alpha',
    0x34: '64-bit 3 Channels Alpha',
    0x35: '80-bit 4 Channels Alpha',
    0x36: '96-bit 5 Channels Alpha',
    0x37: '112-bit 6 Channels Alpha',
    0x38: '128-bit 7 Channels Alpha',
    0x39: '144-bit 8 Channels Alpha',
    0x3a: '64-bit RGBA Half',
    0x3b: '48-bit RGB Half',
    0x3d: '32-bit RGBE',
    0x3e: '16-bit Gray Half',
    0x3f: '32-bit Gray Fixed Point'
}

exifEnumTags['EXIF:Transformation'] = {
    0: 'Horizontal (normal)',
    1: 'Mirror vertical',
    2: 'Mirror horizontal',
    3: 'Rotate 180',
    4: 'Rotate 90 CW',
    5: 'Mirror horizontal and rotate 90 CW',
    6: 'Mirror horizontal and rotate 270 CW',
    7: 'Rotate 270 CW'
}

exifEnumTags['EXIF:Uncompressed'] = {
    0: 'No',
    1: 'Yes'
}

exifEnumTags['EXIF:ImageDataDiscard'] = {
    0: 'Full Resolution',
    1: 'Flexbits Discarded',
    2: 'HighPass Frequency Data Discarded',
    3: 'Highpass and LowPass Frequency Data Discarded'
}

exifEnumTags['EXIF:AlphaDataDiscard'] = {
    0: 'Full Resolution',
    1: 'Flexbits Discarded',
    2: 'HighPass Frequency Data Discarded',
    3: 'Highpass and LowPass Frequency Data Discarded'
}

exifEnumTags['EXIF:USPTOOriginalContentType'] = {
    0: 'Text or Drawing',
    1: 'Grayscale',
    2: 'Color'
}

exifEnumTags['EXIF:CFALayout'] = {
    1: 'Rectangular',
    2: 'Even columns offset down 1/2 row',
    3: 'Even columns offset up 1/2 row',
    4: 'Even rows offset right 1/2 column',
    5: 'Even rows offset left 1/2 column',
    6: 'Even rows offset up by 1/2 row, even columns offset left by 1/2 column',
    7: 'Even rows offset up by 1/2 row, even columns offset right by 1/2 column',
    8: 'Even rows offset down by 1/2 row, even columns offset left by 1/2 column',
    9: 'Even rows offset down by 1/2 row, even columns offset right by 1/2 column'
}

exifEnumTags['EXIF:MarkerNoteSafety'] = {
    0: 'Unsafe',
    1: 'Safe'
}

exifEnumTags['EXIF:ProfileEmbedPolicy'] = {
    0: 'Allow Copying',
    1: 'Embed if Used',
    2: 'Never Embed',
    3: 'No Restrictions'
}

exifEnumTags['EXIF:PreviewColorSpace'] = {
    0: 'Unknown',
    1: 'Gray Gamma 2.2',
    2: 'sRGB',
    3: 'Adobe sRGB',
    4: 'ProPhoto RGB'
}

exifEnumTags['EXIF:ProfileHueSatMapEncoding'] = {
    0: 'Linear',
    1: 'sRGB'
}

exifEnumTags['EXIF:ProfileLookTableEncoding'] = {
    0: 'Linear',
    1: 'sRGB'
}

exifEnumTags['EXIF:DefaultBlackRender'] = {
    0: 'Auto',
    1: 'None'
}

# FIXME but no time for the moment
exifEnumTags['EXIF:GPSLatitudeRef'] = {
    'N': 'N',
    'S': 'S'
}

exifEnumTags['EXIF:GPSLongitudeRef'] = {
    'E': 'E',
    'W': 'W'
}

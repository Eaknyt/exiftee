# -*- coding: utf-8 -*-

import datetime
import os.path

import exiftee
import exiftee.exiftoolext


def _withoutDirs(files):
    ret = []

    for entry in files:
        if not os.path.isdir(entry) and exiftee.fileIsSupported(entry):
            ret.append(entry)

    return ret


def gpsTagNames():
    """
        Retrieves GPS tag names from the EXIF category.

        Returns:
            list: a list of tag names represented as strings.

            Each tag name contains its group name too.
    """
    return ['EXIF:GPSLatitude', 'EXIF:GPSLatitudeRef',
            'EXIF:GPSLongitude', 'EXIF:GPSLongitudeRef']


def getTagNames(files, processFolders=True):
    """
        Retrieves the tag names associated with each file in `files`.

        Args:
            files: A list of file paths. Directories are handled.

            processFolders: A boolean telling the function to process each file
                in the directories contained in `files`.
                This operation is not recursive.

        Returns:
            list: a list of tuples with a tag name as unique element.

            If files is empty, returns an empty list.
    """
    ret = []

    toProcess = []

    if not processFolders:
        toProcess = _withoutDirs(files)
    else:
        toProcess = files

    if toProcess:
        et = exiftee.exiftoolext.Manager.instance().getExifTool()

        datas = et.get_metadata_batch(toProcess)

        for dic in datas:
            # TODO According to some benchmarks, it appears that this is a very
            #  slow operation. Try to find better.
            for fullTag in dic.keys():
                if (fullTag,) not in ret:
                    ret.append((fullTag,))

    return ret


def setTags_batch(ops, files, etInstance=None):
    """
        Execute each tag affection described in `ops` for each file in `files`.

        Args:
            ops: a dict mapping a tag name with a value.

            files: a list of file paths.

        Returns:
            None
    """
    params = []

    for tag, value in ops.items():
        cmdLine = '-{0}={1}'.format(tag, value)
        params.append(bytes(cmdLine, 'utf-8'))

    params.extend([bytes(f, 'utf-8') for f in files])

    et = exiftee.exiftoolext.Manager.instance().getExifTool()
    et.execute(*params)


def setTags(ops, etInstance=None):
    """
        Execute each tag affection described in `ops`.

        Args:
            ops: a dict mapping a ``(file, tag_name)`` tuple with a new value.

            files: a list of files.

        Returns:
            None
    """
    for (file, tag), value in ops.items():
        cmdLine = '-{0}={1}'.format(tag, value)

        params = [bytes(cmdLine, 'utf-8')]
        params.append(bytes(file, 'utf-8'))

    et = exiftee.exiftoolext.Manager.instance().getExifTool()
    et.execute(*params)


def getTags(files, tags):
    """
        Retrieves the values of each tag in `tags` of each file in `files`.

        Args:
            files: a list of file paths.

            tags: a string list of tag names.

        Returns:
            A dict mapping a (file, tag_name) tuple with a current value.
    """
    ret = {}

    et = exiftee.exiftoolext.Manager.instance().getExifTool()

    batch = et.get_tags_batch(tags, files)

    SRC_TAG_NAME = 'SourceFile'

    for tags in batch:
        if len(tags) > 1:
            filePath = tags[SRC_TAG_NAME]

            for tagName, value in tags.items():
                if tagName != SRC_TAG_NAME:
                    ret[(filePath, tagName)] = value

    return ret


def getGpsTags(files):
    """
        Returns the GPS coordinates of each file in `files`.

        Args:
            files: a list of file paths.

        Returns:
            list: a list of dicts mapping a tag name with a value. The gps tags
            and the ``SourceFile`` tag are included.

        .. seealso:: :func:`gpsTagNames`
        .. seealso:: :func:`~exiftool.ExifTool.execute_json`
    """
    et = exiftee.exiftoolext.Manager.instance().getExifTool()

    json = et.get_tags_batch(gpsTagNames(), files)

    return json


def purgeTags(files, etInstance=None):
    """
        Removes all tags from each file in `files`.

        Args:
            files: a string list of file paths.

        Returns:
            None
    """
    params = [bytes('-all=', 'utf-8')]
    params.extend([bytes(f, 'utf-8') for f in files])

    et = exiftee.exiftoolext.Manager.instance().getExifTool()
    et.execute(*params)


def getDates(files):
    """
        Retrieves the creation date time of each file in `files`.

        Args:
            files: a string list of file paths (directories or not).

        Returns:
            dict: a dictionary mapping a file path with a
                :class:`datetime.datetime` object.
    """
    ret = {}

    et = exiftee.exiftoolext.Manager.instance().getExifTool()

    for f in files:
        # Extract the plain tag
        plainDate = et.get_tag('DateTimeOriginal', f)

        # Convert it to datetime
        if not plainDate:
            date = ''
        else:
            date = datetime.datetime.strptime(plainDate,
                                              '%Y:%m:%d %H:%M:%S')

        ret[f] = date

    return ret


def setDate(date, control, files, etInstance=None):
    """
        Modifies the creation date time tags of each file in `files`.

        Args:
            date: a :class:`datetime.datetime` object or a tuple of six
                integers.

            control: a string with a value in the following: ::

                '+' -- the dates will be shifted forward.
                '-' -- the dates will be shifted backward.
                ''  -- the dates are simply set.

            files: a string list of file paths (directories or not).

        Returns:
            None
    """
    assert(control == '-' or control == '+' or not control)

    if type(date) is datetime.datetime:
        dateFormatted = date.strftime('%Y:%m:%d %H:%M:%S')
    else:
        dateFormatted = date

    cmdLine = '-DateTimeOriginal{0}={1}'.format(control, dateFormatted)
    params = [bytes(cmdLine, 'utf-8')]

    params.extend([bytes(f, 'utf-8') for f in files])

    et = exiftee.exiftoolext.Manager.instance().getExifTool()
    et.execute(*params)


def getMetadataByCat(groups, files):
    """
        Retrieves the tags of each group in `groups`.

        Args:
            groups: a string list of EXIF groups.
                .. _See: http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/

            files: a string list of file paths (directories or not).

        Returns:
            list: a list of dicts mapping a tag name with a value.

        .. seealso:: :func:`~exiftool.ExifTool.execute_json`
    """
    params = [bytes('-{0}:all'.format(g), 'utf-8') for g in groups]
    params.extend([bytes(f, 'utf-8') for f in files])

    et = exiftee.exiftoolext.Manager.instance().getExifTool()

    ret = et.execute_json(*params)

    return ret


def getTagNamesByCat(groups, files, processFolders=True, asTuple=False):
    """
        Retrieves the tag names from each group in `groups` in each file in
        `files`.

        Args:
            groups: a string list of group names.
                .. _See: http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/

            files: a string list of file paths.

            processFolders: A boolean telling the function to process each file
                in the directories contained in `files`.
                This operation is not recursive.

        Returns:
            list: a string list of tag names.

        .. todo:: remove the `asTuple` arg.
    """
    if processFolders:
        toProcess = files
    else:
        toProcess = _withoutDirs(files)

    if not toProcess:
        return []

    ret = []

    json = getMetadataByCat(groups, toProcess)

    for result in json:
        for tagName, tagValue in result.items():
            if asTuple:
                toAppend = (tagName, )
            else:
                toAppend = tagName

            if toAppend not in ret:
                ret.append(toAppend)

    return ret


def sort(tag, files, ascending):
    """
        Retrieves a list of file paths sorted by the tag value of `tag`.

        The sort is applied to `files` and the sort mode is specified by
        `ascending`.

        Since this function aims to help to display sorted images, any
        directory path contained in 'files' is ignored.

        Args:
            tag: a string containing a fully-specified tag name.

            files: a string list of file paths.

            ascending: a boolean which is True if the sort must be ascending,
                or False if it must be descending.

        Returns:
            list: a list containing (file_path, sort_tag_value) tuples.
    """
    if not tag:
        return [(file, '') for file in files]

    toProcess = []
    folders = []

    for entry in files:
        if os.path.isdir(entry):
            folders.append(entry)
        else:
            toProcess.append(entry)

    if not toProcess:
        return [(file, '') for file in files]

    sortCriteria = tag

    if not ascending:
        sortCriteria = '-' + tag

    # Build the command line
    params = [bytes('-{0}'.format(tag), 'utf-8')]
    params.append(bytes('-fileOrder', 'utf-8'))
    params.append(bytes(sortCriteria, 'utf-8'))

    params.extend([bytes(f, 'utf-8') for f in toProcess])

    # Retrieve the results and build the sorted list
    et = exiftee.exiftoolext.Manager.instance().getExifTool()

    json = et.execute_json(*params)

    ret = []

    for result in json:
        value = None

        if tag in result:
            value = result[tag]

        ret.append((result['SourceFile'], value))

    for fol in folders:
        if ascending:
            ret.insert(0, (fol, None))
        else:
            ret.append((fol, None))

    return ret


def rename(pattern, files, etInstance=None):
    """
        Rename the files from `files` with the pattern defined by `pattern`.

        The pattern syntax is the one used by
        .. _exiftool: http://owl.phy.queensu.ca/~phil/exiftool/filename.html

        Args:
            pattern:

            files:

        Returns:
            None
    """
    if '$' in pattern:
        cmdLine = '-FileName<{0}'.format(pattern)
    else:
        cmdLine = '-FileName={0}'.format(pattern)

    params = [bytes(cmdLine, 'utf-8')]
    params.extend([bytes(f, 'utf-8') for f in files])

    et = exiftee.exiftoolext.Manager.instance().getExifTool()
    et.execute(*params)

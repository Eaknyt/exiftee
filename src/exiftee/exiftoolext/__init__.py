# -*- coding: utf-8 -*-

"""
    The ``exiftee.exiftoolext`` package is a wrapper for various exiftool
    operations.

    It may be used to retrieve data in order to populate some views from the
    ``exiftee.exifviews`` package, or to set specific tags.
"""
from exiftee.exiftoolext.funcs import getGpsTags
from exiftee.exiftoolext.funcs import getTagNames
from exiftee.exiftoolext.funcs import gpsTagNames
from exiftee.exiftoolext.funcs import setTags_batch
from exiftee.exiftoolext.funcs import setTags
from exiftee.exiftoolext.funcs import getTags
from exiftee.exiftoolext.funcs import purgeTags
from exiftee.exiftoolext.funcs import getDates
from exiftee.exiftoolext.funcs import setDate
from exiftee.exiftoolext.funcs import getMetadataByCat
from exiftee.exiftoolext.funcs import getTagNamesByCat
from exiftee.exiftoolext.funcs import sort
from exiftee.exiftoolext.funcs import rename

from exiftee.exiftoolext.lookuptables import *

from exiftee.exiftoolext.manager import Manager

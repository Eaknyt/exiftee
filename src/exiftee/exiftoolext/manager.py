# -*- coding: utf-8 -*-

import exiftool


class Manager(object):
    """
        The ``Manager`` singleton is the entry point to all interactions with
        exiftool.

        Whenever the use of the :class:`exiftool.ExifTool` class is required,
        use :func:`exiftee.exiftoolext.Manager.getExifTool` to retrieve a
        reference to an ExifTool object. Only one instance of ExifTool can
        run at the same time. Call :func:`exiftee.exiftoolext.Manager.finish`
        to manually terminate the current ExifTool instance.
    """
    _instance = None
    _etInstance = None

    def __init__(self):
        if Manager._instance:
            raise NotImplemented('A singleton cannot be instanciated.')

    @staticmethod
    def instance():
        """
            Retrieves the ``Manager`` singleton instance.

            Returns:
                :class:`exiftee.exiftoolext.Manager`
        """
        if not Manager._instance:
            Manager._instance = Manager()

        return Manager._instance

    def getExifTool(self):
        """
            Retrieves the current instance of ExifTool.

            If it doesn't exist, create the subprocess and start it. Otherwise
            just returns the existing instance.

            Returns:
                :class:`exiftool.ExifTool`
        """
        if not Manager._etInstance:
            Manager._etInstance = exiftool.ExifTool()

        if not Manager._etInstance.running:
            Manager._etInstance.start()

        return Manager._etInstance

    def finish(self):
        """
            Terminates the current ExifTool subprocess.

            Returns:
                None
        """
        if Manager._etInstance.running:
            Manager._etInstance.terminate()

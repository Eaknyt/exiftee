# -*- coding: utf-8 -*-

import tkinter as tk
import tkinter.ttk as ttk

import exiftee.tk


class CTreeView(ttk.Treeview):
    """
        The ``CTreeView`` provides a tkinter tree view with embedded scrollbars
        and filtering abilities.

        The model way
        -------------
        Firstly, you should not call ``insert()`` directly on a ``CTreeView``
        instance. Prefer dealing with the model attribute.

        When you filled the model, call ``update()`` to refresh the tree
        content.

            .. seealso:: TableSortFilterModel

        Filtering
        ---------
        The ``filterVar`` attribute is a ``tk.StringVar()`` that describes the
        regular expression used to filter the tree view.

        Each item that doesn't match this expression, except the top level
        items, is not displayed.

        Let's suppose you want to filter the content of the tree view with
        an Entry.

            # Create the tree
            [...]

            filterEntry = tk.Entry(parent, tree.filterVar)

        Now, whenever the user changes the content of ``filterEntry``, the tree
        view is updated accordingly.

        Instead of a tk.Entry, you can use a ValidateEntry from the same
        package.

            .. seealso:: ValidateEntry
    """
    def __init__(self, parent, **kwargs):
        ttk.Treeview.__init__(self, parent, **kwargs)

        self.model = exiftee.tk.TableSortFilterModel()

        # Add scrollbars to the tree view
        yScrollBar = ttk.Scrollbar(self, orient=tk.VERTICAL)
        yScrollBar.pack(side=tk.RIGHT, fill=tk.Y)

        self.config(yscrollcommand=yScrollBar.set)
        yScrollBar.config(command=self.yview)

        xScrollBar = ttk.Scrollbar(self, orient=tk.HORIZONTAL)
        xScrollBar.pack(side=tk.BOTTOM, fill=tk.X)

        self.config(xscrollcommand=xScrollBar.set)
        xScrollBar.config(command=self.xview)

        # Resize the first column
        self.column('#0', minwidth=15, width=15, stretch=tk.NO)

        # Create a var for filtering
        self.filterVar = tk.StringVar()

        #  No command option in the Entry class ! ...sigh
        self.filterVar.trace('w',
                             lambda name, index, mode,
                             filterVar=self.filterVar:
                             self._onFilterVarChanged(filterVar))

    def _onFilterVarChanged(self, var):
        """
            Filter the model using the regular expression 'var' and update
            the tree view.

            This function is called when the value of the ``filterVar``
            attribute changes.
        """
        filterExpr = var.get()

        pattern = '(.*:' + filterExpr + '.*)|' + filterExpr + '.*'

        self.model.setRegex(pattern)
        self.update()

    def _fill(self):
        """
            Fill the tree using the proxy of the model.
        """
        for row in self.model.proxy():
            size = len(row)

            if size < 2:
                raise ValueError('A row size must contain at least 2 elements')

            parent = row[0]
            itemId = row[1]

            self.insert(parent, tk.END, itemId, open=True, values=row[2:size])

    def clear(self):
        """
            Removes all items from the tree.
        """
        # Clear the tree view
        self.delete(*self.get_children())

    def update(self):
        """
            Clear the tree content and fill it with the content provided by the
            model.
        """
        self.clear()
        self._fill()

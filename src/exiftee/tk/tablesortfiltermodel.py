# -*- coding: utf-8 -*-

import re


class TableSortFilterModel(object):
    """
        The ``TableSortFilterModel`` provides sorting and filtering features
        on a list of tuples.
    """
    def __init__(self):
        self._source = []
        self._proxy = []

        self.enableSorting = True

        # Filtering
        self.column = 1
        self._regex = None

    def _sortAndFilter(self):
        self._proxy = self._source.copy()

        self._filter()

        if self.enableSorting:
            self._sort()

    def _filter(self):
        if self._regex:
            toRemove = []

            for tup in self._proxy:
                val = tup[self.column]

                # Ignore top level items
                #TODO handle nested items
                if not tup[0]:
                    continue

                if not self._regex.fullmatch(val):
                    toRemove.append(tup)

            for v in toRemove:
                self._proxy.remove(v)

    def _sort(self):
        self._proxy = (sorted(self._proxy, key=lambda tup: tup[self.column]))

    @property
    def source(self):
        """
            The source property defines the source tuple list containing the
            data to sort or/and filter.
        """
        return self._source

    @source.setter
    def source(self, newSource):
        if self._source != newSource:
            self._source = newSource

            self._sortAndFilter()

    @source.deleter
    def source(self):
        self._source = []
        self._proxy = []

    def proxy(self):
        """
            Returns the sorted and filtered table.
        """
        return self._proxy

    def setRegex(self, pattern):
        if not self._regex or (self._regex.pattern != pattern):
            realPattern = pattern
            self._regex = re.compile(realPattern, re.IGNORECASE)

            self._sortAndFilter()

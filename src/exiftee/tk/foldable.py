# -*- coding: utf-8 -*-

import tkinter as tk


class Foldable(tk.Frame):
    """
        The ``Foldable`` class provides a expandable/collapsible panel widget.


        Add widgets to the sub frame
        ----------------------------

        Use the ``placeholder`` attribute to add widgets to the Foldable panel.
        This attribute is a basic tkinter Frame.
    """
    def __init__(self, parent=None, **kwargs):
        tk.Frame.__init__(self, parent, **kwargs)

        # Title frame
        titleFrame = tk.Frame(self)
        titleFrame.pack(side=tk.TOP, fill=tk.X)

        # Title frame label
        self._textVar = tk.StringVar()

        titleLabel = tk.Label(titleFrame,
                              textvariable=self._textVar)
        titleLabel.pack(side=tk.LEFT, fill=tk.X)

        # Title frame button
        self._toggleVar = tk.BooleanVar()
        self._toggleVar.set(0)

        self._toggleBtnTextVar = tk.StringVar()
        self._toggleBtnTextVar.set('-')

        toggleBtn = tk.Checkbutton(titleFrame,
                                   width=2,
                                   indicatoron=0,
                                   variable=self._toggleVar,
                                   textvariable=self._toggleBtnTextVar,
                                   command=self.toggle)
        toggleBtn.pack(side=tk.RIGHT)

        # The placeholder frame is the foldable content
        self.placeholder = tk.Frame(self, relief=tk.SUNKEN, borderwidth=1)
        self.placeholder.pack(side=tk.TOP, padx=4, fill=tk.BOTH, expand=True)

    @property
    def text(self):
        """
            The text property defines the text of the label displayed by the
            Foldable's title frame.
        """
        return self._textVar.get()

    @text.setter
    def text(self, newText):
        if self._textVar.get() != newText:
            self._textVar.set(newText)

    @text.deleter
    def text(self):
        self._textVar.set('')

    def toggle(self):
        # Update the toggle button's text
        if self._toggleBtnTextVar.get() == '+':
            self._toggleBtnTextVar.set('-')
        elif self._toggleBtnTextVar.get() == '-':
            self._toggleBtnTextVar.set('+')

        # Wraps or unwraps the placeholder frame
        # (1) Wow that's far easier than QtQuick
        if self._toggleVar.get():
            self.placeholder.forget()
        else:
            self.placeholder.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

# -*- coding: utf-8 -*-

import calendar
import datetime
import tkinter as tk
import tkinter.ttk as ttk


class DateEntry(tk.Frame):
    """
        The ``DateEntry`` widget provides a horizontal set of combo boxes to
        display a date.

        The current date can be changed programmatically using the date
        property.
    """
    def __init__(self, parent=None, date=None, **kwargs):
        """

        """
        tk.Frame.__init__(self, parent, **kwargs)

        self._date = date

        if not self._date:
            self._date = datetime.datetime.now().date()

        self._setupUI()

    def _setupUI(self):
        if self._date:
            currentYear = self._date.year
            currentMonth = self._date.month - 1
        else:
            currentYear = datetime.datetime.now().year
            currentMonth = datetime.datetime.now().month - 1

        self._dayCbox = ttk.Combobox(self,
                                     values=self.daysOfMonth(currentYear,
                                                             currentMonth))
        self._dayCbox.bind('<<ComboboxSelected>>', self._daySelected)
        self._dayCbox.grid(row=0, column=0)

        self._monthCbox = ttk.Combobox(self, values=self.monthsOfYear())
        self._monthCbox.bind('<<ComboboxSelected>>', self._monthSelected)
        self._monthCbox.grid(row=0, column=1)

        self._yearCbox = ttk.Combobox(self, values=self.years())
        self._yearCbox.bind('<<ComboboxSelected>>', self._yearSelected)
        self._yearCbox.grid(row=0, column=2)

        self._updateWidgets()

    def _daySelected(self, event=None):
        self._date = self._date.replace(day=int(self._dayCbox.get()))

    def _monthSelected(self, event=None):
        self._date = self._date.replace(month=self._monthCbox.current() + 1)
        self._updateDayCboxContent()

    def _yearSelected(self, event=None):
        self._date = self._date.replace(year=int(self._yearCbox.get()))
        self._updateDayCboxContent()

    def _updateDayCboxContent(self):
        self._dayCbox.config(values=self.daysOfMonth(self._date.year,
                                                     self._date.month))

    def _updateWidgets(self):
        """
            Update the combo boxes to match the current date.

            The content of the day combo box may be adjusted depending on the
            current month and year.
        """
        self._dayCbox.set(self._date.day)
        self._monthCbox.current(self._date.month - 1)
        self._yearCbox.set(self._date.year)

    @classmethod
    def years(self):
        """
            Returns a string list of available years.
        """
        return [str(i) for i in range(1920, datetime.datetime.now().year + 1)]

    @classmethod
    def monthsOfYear(self):
        """
            Returns a string list of the twelve months of the year.

            We don't use the calendar API because we need the month names to be
            translatables.
        """
        return [_('January'), _('February'), _('March'), _('April'), _('May'),
                _('June'), _('July'), _('August'), _('September'),
                _('October'), _('November'), _('December')]

    @classmethod
    def daysOfMonth(self, year, month):
        """
            Returns a string list of the days in the month 'month' of the year
            'year'.
        """
        return [str(i)
                for i in range(1, calendar.monthrange(year, month)[1] + 1)]

    @property
    def date(self):
        """
            The date property represents the date object displayed by the
            DateEntry.
        """
        return self._date

    @date.setter
    def date(self, newDate):
        if self._date != newDate:
            self._date = newDate

            self._updateWidgets()

    @date.deleter
    def date(self):
        self._date = datetime.now().date()

    def setState(self, state):
        """
            because of the tkinter's amazing state logic
        """
        self._dayCbox.config(state=state)
        self._monthCbox.config(state=state)
        self._yearCbox.config(state=state)

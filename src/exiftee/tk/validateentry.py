# -*- coding: utf-8 -*-

import tkinter as tk


class ValidateEntry(tk.Entry):
    """
        The ``ValidateEntry`` provides a tkinter Entry with validation
        facilities.

        Set the validateCallback property to add validation. The provided
        function must have two arguments : the first will contain the text the
        entry will have if the change is accepted. The second will contain the
        character inserted.
    """
    def __init__(self, parent, **kwargs):
        tk.Entry.__init__(self, parent, **kwargs)

        self._callback = None
        self._okCommand = None

        self.config(validate='key')

    @property
    def validateCallback(self):
        return self._callback

    @validateCallback.setter
    def validateCallback(self, callback):
        if self._callback != callback:
            self._callback = callback

            self._okCommand = self.register(self._callback)

            self.config(validatecommand=(self._okCommand, '%P', '%S'))

# -*- coding: utf-8 -*-

import gettext
import os
import sys
import tkinter as tk
import tkinter.messagebox

# Add the source directory to the python path
sys.path.insert(0, os.path.abspath('src'))

# Custom imports
import exiftee
import exiftee.core


class MyCallWrapper:
    """
        Due to ExifTool which runs during all the exiftee execution, a custom
        tkinter call wrapper is required to catch the BrokenPipeException.
    """
    def __init__(self, func, subst, widget):
        self.func = func
        self.subst = subst
        self.widget = widget

    def __call__(self, *args):
        try:
            if self.subst:
                args = self.subst(*args)

            return self.func(*args)
        except SystemExit as msg:
            raise SystemExit(msg)
        except BrokenPipeError:
            logMsg = 'ExifTool was terminated. Exit.'
            print(logMsg, file=sys.stderr)

            errorMsg = 'ExifTool was terminated. The software will now exit.'
            tk.messagebox.showerror(title=_('Critical error'),
                                    message=errorMsg)

            sys.exit(1)

if __name__ == '__main__':
    # Create the local folder if it not exists
    localDir = exiftee.localPath()

    if not os.path.exists(localDir):
        os.makedirs(localDir)

    # Init the i18n engine
    gettext.install('exiftee', exiftee.i18nPath())

    #
    tk.CallWrapper = MyCallWrapper

    # Open the main window
    mw = exiftee.core.MainWindow()

    try:
        tk.mainloop()
    except AttributeError:
        errorMsg = 'Non-critical exception because of the Welcome dialog. ' \
                   'Don\'t close the main window directly with the Welcome ' \
                   'dialog open.'

        print(errorMsg, file=sys.stderr)
